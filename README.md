# Anomaly detection in waveforms

Investigation of common anomaly detection methods to a variety of data sets

## Prerequisites
* Python version: 3.10
* Use the requirements.txt to deploy the correct dependencies.

## Synthetic Data generation Folder
* To produce labeled time series data

## Tools Folder
* data_preprocessing.py - Used to preprocess time sequence to pulse width histograms
* iforest.py - Testing with isolation forest model
* model.py - Contains different architecture of variational autoencoder

## anamoly explorere app Folder
* visualises common anomaly detection methods
* UI uses streamlit as framework
