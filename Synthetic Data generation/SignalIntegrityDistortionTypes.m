classdef SignalIntegrityDistortionTypes < int8
    enumeration
        Noise               (1) 
        RandomCrossTalk     (2) 
        DutyCycleDistortion (3) 
        RandomRunt          (4) 
        RandomGlitch        (5) 
        Reflections         (6) 
        RandomJitter        (7) 
        
        
    end
end