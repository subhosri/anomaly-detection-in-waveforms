classdef ModulationSignalGenerator < handle
    
    
    properties(GetAccess = public, SetAccess = private)
        
        AM_factor
        FM_factor
        
    end    %properties(GetAccess = public, SetAccess = private)
    
    properties(GetAccess = public, SetAccess = public)
        NoiseAlwaysON;        
        TargetNrOfSamples;                
    end     %properties(GetAccess = public, SetAccess = public)
    
    
    methods (Access = public)
        
        %creator
        %These are default values.  For generation of test data with 'MakeTrainingData'
        %each of the parameters are randomized.
        function obj = ModulationSignalGenerator
            obj.NoiseAlwaysON           = 1;
            
            obj.NoiseON                 = 1;
            obj.RandomCrossTalkON       = 0;
            obj.DutyCycleDistortionON   = 0;
            obj.RandomRuntON            = 0;
            obj.RandomGlitchON          = 0;
            obj.ReflectionsON           = 0;
            obj.RandomJitterON          = 0;
            
            obj.WindowingON             = 0;    %default windowing is off
            obj.WindowStart             = 4e5;    %in samples
            obj.WindowEnd               = 5e5;    %in samples
            
            obj.NoiseStdDev             = 0.02;
            
            obj.TargetNrOfSamples       = 1e6;
            
            %obj.NrOfPulses              = 1000;
            obj.AvgPulseWidth           = 50;
            
            %obj.ChannelImpulseResponse  = exp(0:-0.2:-2);
            obj.ChannelImpulseResponse  = exp(0:-0.1:-4);
            
            obj.RUNT_probability        = 1e-2;
            obj.RUNT_scaling_limits     = [0.2 0.8];
            obj.RUNT_count              = 0;
            obj.RUNT_scale_count        = 0;
            
            obj.GLITCH_probability      = 1e-2;
            obj.GLITCH_count            = 0;
            obj.GLITCH_windex_count     = 0;
            
            obj.CROSSTALK_amp           = 0.05;
            obj.CROSSTALK_width         = 10;
            
            obj.DCD_imbalance           = 10;
            
            obj.REFLECTIONS_h_refl      = [1,zeros(1,30),0.15];
            %obj.REFLECTIONS_h_refl      = [1,zeros(1,20),0.05];
            
            obj.JITTER_stddev           = 5;
        end
        
        function ClearDistortionTypes(obj)
            obj.NoiseON                 = 0;
            obj.RandomCrossTalkON       = 0;
            obj.DutyCycleDistortionON   = 0;
            obj.RandomRuntON            = 0;
            obj.RandomGlitchON          = 0;
            obj.ReflectionsON           = 0;
            obj.RandomJitterON          = 0;
        end
        
        function AddDistortionType_Exclusive(obj,dist_type)
            
            obj.ClearDistortionTypes
            obj.SetDistortionType(dist_type,1)
            
            if obj.NoiseAlwaysON
                obj.SetDistortionType(SignalIntegrityDistortionTypes.Noise,1);
            end
        end
        
        function SetDistortionType(obj,dist_type,val)
            
            switch dist_type
                case SignalIntegrityDistortionTypes.Noise
                    obj.NoiseON  = val;
                case SignalIntegrityDistortionTypes.RandomCrossTalk
                    obj.RandomCrossTalkON  = val;
                case SignalIntegrityDistortionTypes.DutyCycleDistortion
                    obj.DutyCycleDistortionON  = val;
                case SignalIntegrityDistortionTypes.RandomRunt
                    obj.RandomRuntON  = val;
                case SignalIntegrityDistortionTypes.RandomGlitch
                    obj.RandomGlitchON  = val;
                case SignalIntegrityDistortionTypes.Reflections
                    obj.ReflectionsON  = val;
                case SignalIntegrityDistortionTypes.RandomJitter
                    obj.RandomJitterON  = val;
            end
            
        end
        
        function SetWindowing_ON_OFF(obj,on_off_switch)
            obj.WindowingON             = on_off_switch;
        end
        
        function SetWindowing_StartEnd(obj,window_start,window_end)
            
            if window_start > 0 && window_start<obj.TargetNrOfSamples
                obj.WindowStart = window_start;
            else
                disp('invalid value for window start')
            end
            
            if window_end > 0 && window_end<=obj.TargetNrOfSamples && window_end >= window_start
                obj.WindowEnd   = window_end;
            else
                disp('invalid value for window start')
            end
        end
        
        
        function SetNoiseLevel(obj,noise_stddev)
            obj.NoiseStdDev = noise_stddev;
        end
        
        function SetRuntProperties(obj,prob,amp_min,amp_max)
            obj.RUNT_probability = prob;
            obj.RUNT_scaling_limits = [amp_min,amp_max];
            obj.RUNT_count = 0;
            obj.RUNT_scale_count = 0;
        end
        
        function SetCrossTalkProperties(obj,amp,width)
            obj.CROSSTALK_amp = amp;
            obj.CROSSTALK_width = width;
        end
        
        function SetDCDProperties(obj,imbalance)
            obj.DCD_imbalance = imbalance;
        end
        
        function SetGlitchProperties(obj,prob)
            obj.GLITCH_probability = prob;
            obj.GLITCH_count = 0;
            obj.GLITCH_windex_count = 0;
        end
        
        function SetReflectionProperties(obj,h_refl)
            obj.REFLECTIONS_h_refl = h_refl;
        end
        
        function SetJitterProperties(obj,jsd)
            obj.JITTER_stddev=jsd;
        end
        
        function w = generate_sequence(obj)
            x = [];
            pulse_width_base = obj.AvgPulseWidth;
            pulse_amplitude_default = obj.PulseAmplitude_Default;
            
            current_sample_position = 0;
            k = 0;  %pulse counter
            while current_sample_position < obj.TargetNrOfSamples
                %for k = 1:obj.NrOfPulses
                k = k+1;
                             
                pulse_amplitude = sign(randn(1))*pulse_amplitude_default; %generates randomly falling/rising flanks                           
                pulse_width = pulse_width_base;
                
                if obj.DutyCycleDistortionON
                    pulse_width_deviation = obj.DCD_imbalance*(-2*mod(k,2)+1); %in alternating manner the pulse width gets increased or decreased by DCD_imbalance
                    pulse_width = pulse_width + pulse_width_deviation;
                end
                
                if obj.RandomJitterON
                    pulse_width_deviation = round(obj.JITTER_stddev*rand(1));
                    pulse_width = pulse_width + pulse_width_deviation;
                end
                
                this_new_pulse = pulse_amplitude*ones(1,pulse_width);
                
                if obj.RandomGlitchON
                    %some random pulses have a glitch -> split pulse into mini pulses
                    if rand(1)<obj.GLITCH_probability
                        windex = sort(randi([10,pulse_width-10],1,2));
                        %generate 2 random numbers within bounds of width
                        %change the sign of signal between these two indicies.
                        this_new_pulse(windex(1):windex(2))=-pulse_amplitude;
                        obj.GLITCH_count = obj.GLITCH_count+1;
                    end
                end
                
                if obj.RandomRuntON
                    %some random pulses are runts -> reduce amplitude
                    if rand(1)<obj.RUNT_probability
                        this_scale = obj.RUNT_scaling_limits(1)+rand(1)*(obj.RUNT_scaling_limits(2)-obj.RUNT_scaling_limits(1));
                        this_new_pulse = this_scale * this_new_pulse;
                        obj.RUNT_count = obj.RUNT_count + 1;
                    end
                    
                end
            
                
                %just concatenate this new pulse
                x = [x,this_new_pulse];
                current_sample_position = length(x);
            
                
            end
            
            
            
            
            
            
            %alwayse use channel impulse response
            h = obj.ChannelImpulseResponse;
            %h is easier to work with
            y = obj.apply_filter(x,h); %filtering to resolve the instantaneous transitions of x
            
            if obj.ReflectionsON
                %if reflections on, then apply additional filter
                h_refl = obj.REFLECTIONS_h_refl;
                z_tmp = obj.apply_filter(y,h_refl);
                
                %In the most basic case of windowing, reflections are switched on
                %suddenly.
                %Thus we have:
                if obj.WindowingON
                    %the the convolution into account -> we want a smooth
                    %transition from 'no effect'.
                    z_tmp_shift = [zeros(1,length(h_refl)-1),z_tmp];
                    z = y;
                    z(obj.WindowStart:obj.WindowEnd)=z_tmp_shift(obj.WindowStart:obj.WindowEnd);
                else
                    z = z_tmp;
                end
                
            else
                z = y;
            end
            
            
            if obj.NoiseON
                z = z + obj.NoiseStdDev*randn(1,length(z));
            end
            
            
            if obj.RandomCrossTalkON
                width_cross_talk = obj.CROSSTALK_width;
                amplitude_cross_talk = obj.CROSSTALK_amp;
                %first generate a full length crosstalk signal
                w_int = amplitude_cross_talk*obj.generate_RBS(width_cross_talk,length(z));
                
                w = w_int + z;

            else
                w = z;
            end
            
            
            
        end
        
        
    end    %methods (Access = public)
    
    methods (Access = public, Static)%(Access = private, Static)
        
        function y = apply_filter(x,h)
            h = h/sum(h);
            y = conv(h,x);
            y = y(length(h):(length(x)));
        end
        
        
        function x = generate_RBS(pulse_width,total_length)
            
            NrOfPeriods = floor(total_length / pulse_width);
            x=[];
            for k = 1:NrOfPeriods
                this_pulse = sign(randn(1))*ones(1,pulse_width);
                x = [x,this_pulse];
            end
            
            remaining_length = total_length - length(x);
            last_pulse = sign(randn(1))*ones(1,remaining_length);
            x = [x,last_pulse];
            
        end
        
        
    end %methods (Access = private, Static)
    
end