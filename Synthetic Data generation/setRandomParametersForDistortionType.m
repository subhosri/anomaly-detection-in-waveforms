function mtsg = setRandomParametersForDistortionType(mtsg,this_distortion_type)
%This function was created to ensure that training data and test data are
%generated with the same range of randomly generated parameters.

%Index: distortion type number
%mtsg: signal generator object which has the method 'Set<Type>Properties'


switch this_distortion_type
    case SignalIntegrityDistortionTypes.RandomRunt
        %runt_prob = 10^(-1.5-1.5*rand(1));
        runt_prob = 10^(-1-1*rand(1));
        runt_scale_range = 0.01+0.9*rand(1,2);   %2 numbers between 0.1 and 0.9.
        runt_scale_min = min(runt_scale_range);
        runt_scale_max = max(runt_scale_range);        
        mtsg.SetRuntProperties(runt_prob,runt_scale_min,runt_scale_max);
        
    case SignalIntegrityDistortionTypes.RandomCrossTalk
        ct_amp = 0.01+0.1*rand(1);
        ct_width = 1+randi([1,45]);
        mtsg.SetCrossTalkProperties(ct_amp,ct_width);
        
    case SignalIntegrityDistortionTypes.DutyCycleDistortion
        dcd_imbalance = randi([1 15]);  %!!currently fitting to default width of 50
        mtsg.SetDCDProperties(dcd_imbalance);
        
    case SignalIntegrityDistortionTypes.RandomGlitch
        glitch_prob = 10^(-1-1*rand(1));
        mtsg.SetGlitchProperties(glitch_prob);
        h_refl = [1,zeros(1,randi([1 45])),(0.01+0.1*rand(1))];
        h_refl = h_refl / sum(h_refl);
        mtsg.SetReflectionProperties(h_refl);
        
    case SignalIntegrityDistortionTypes.Reflections
        %random delay and random amplitude of reflection.
        h_refl = [1,zeros(1,randi([1 45])),(0.01+0.1*rand(1))];
        h_refl = h_refl / sum(h_refl);
        mtsg.SetReflectionProperties(h_refl);

        
    case SignalIntegrityDistortionTypes.RandomJitter
        mtsg.SetJitterProperties(randi([1 10]));
        dcd_imbalance = randi([1 15]);  %!!currently fitting to default width of 50
        mtsg.SetDCDProperties(dcd_imbalance);
end

