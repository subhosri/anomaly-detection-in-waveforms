classdef ModulationSignalTypes < int8
    enumeration
        AM                  (1) 
        FM                  (2)                
    end
end