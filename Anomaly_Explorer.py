import streamlit as st

import app.streamlit_utils as st_utils

st.set_page_config(page_title="Anomaly Explorer App",
                   page_icon="👋",
                   layout='wide')

st.write("# Welcome to the Anomaly Explorer App 👋")

st.sidebar.success("Select a page above.")

st_utils.inject_background_style()

st.markdown("""
    Select on the sidebar the page to enter.
    * Training: Train a model on a selected dataset.
    * Exploration: Explore the saved models latent spaces.
""")
