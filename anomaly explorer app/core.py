"""This module contains wrappers for all functions to be cached with
streamlit cache_data and cache_resource, from core.app_core and core.plotter.
"""
import logging

import streamlit as st

import core.app_core as core
import core.data as data
import core.plotter as plotter
import core.utils as utils


@st.cache_resource(show_spinner='Loading model...')
def load_model(model_path):
    """Load keras from model folder.

    :param model_path: Path to model folder
    :return: Loaded model.
    """
    return core.load_model(model_path)


@st.cache_data(show_spinner='Loading model metadata...')
def load_model_metadata(model_path):
    """Load metadata associated to a model

    :param model_path: Path to model folder
    :return: dict with metadata and train history dict.
    """
    return core.load_model_metadata(model_path)


@st.cache_data(show_spinner='Loading dataset...')
def get_dataset(dataset_name, **kwargs):
    """Get dataset.

    :param dataset_name: Name of dataset
    :param kwargs: Keyword args passed to Dataset constructor.
    :raises ValueError: if dataset_name is not found.
    :return: tuple (Dataset instance, Loaded dataset config)
    """
    return data.load_dataset(dataset_name, **kwargs)


@st.cache_data(show_spinner='Preparing data...')
def prepare_data(dataset_config, _dataset,
                 target_distrib=('class', {'normal': 1}),
                 split=True, test_size=0.2, val_random_state=123,
                 columns=None, standard_scaler=False, fit_on_train=True,
                 fit_scaler_on={'class': ['normal']}):
    """Prepare data, including splitting data, fitting scaler and scaling
        data.

        :param dataset_config: Dataset configuration dict.
            Used to evaluate streamlit cache hit.
        :param _dataset: Dataset class instance.
            Ignored in the streamlit cache hit evaluation.
        :param target_distrib: Tuple with the labels column to filter on and a
            dict of desired values proportion.
        :param split: Whether to split data into train, val and test.
            Defaults to True.
        :param test_size: Split size for test set. Defaults to 0.2.
        :param val_random_state: Random seed for train/validation split.
            Defaults to 123.
        :param columns: List of columns to use. Defaults to None,
            using all data columns.
        :param standard_scaler: Whether to use Standard scaler, or
            MinMaxScaler if False. Defaults to False (MinMaxScaler)
        :param fit_on_train: Whether to fit scaler only on train data.
            Defaults to True.
        :param fit_scaler_on: Dict with class and/or group values to filter
            the data for fitting the scaler. Defaults to {'class': ['normal']},
            so scaler will be fitted only on data from class normal.
            Note that class column only allows `normal` or `abnormal`.
    """
    _dataset.prepare_data(split, test_size, val_random_state, columns, standard_scaler, fit_on_train, fit_scaler_on)
    return _dataset


@st.cache_data(show_spinner=False)
def convert_to_numpy(X):
    """Convert Dataframe to numpy array, extracting the expected shape from
    the index structure.

    :param X: Dataframe to convert
    :return: Numpy ndarray.
    """
    return utils.convert_to_numpy(X)


@st.cache_data(show_spinner='Computing results...')
def compute_results(model_path, _model, X, _scaler, anomaly_criteria_metric):
    """Get the model reconstructions for data and anomaly scores.

    :param model_path: Path to model folder.
        Used to evaluate streamlit cache hit.
    :param _model: Keras model to use.
        Ignored in the streamlit cache hit evaluation.
    :param X: (Scaled) input data to model.
    :param _scaler: Scaler to inverse transform the reconstructions.
        Ignored in the streamlit cache hit evaluation.
    :param anomaly_criteria_metric: Anomaly criteria metric to use,
        defaults to 'MSE'
    :raises ValueError: If Anomaly criteria is unknown.
    :return: Reconstructions dataframe with latent space, and anomaly scores.
    """
    return core.compute_results(_model, X, _scaler, anomaly_criteria_metric)


@st.cache_data(show_spinner='Computing anomaly scores...')
def compute_anomaly_scores(reconstructions, anomaly_criteria, anomaly_levels):
    """Compute the anomaly score(s), from the anomaly criteria values
    for the given levels. Finally join them to the data.

    :param reconstructions: Reconstruction dataframe.
    :param anomaly_criteria: Anomaly criteria dataframe.
    :param anomaly_levels: Anomaly levels to compute, defaults to 'sample'
    :return: Reconstructions dataframe with anomaly scores.
    """
    return core.compute_anomaly_scores(
        reconstructions, anomaly_criteria, anomaly_levels)


@st.cache_data(show_spinner='Getting latent boundaries...')
def get_latent_limits(data):
    """Get default grid configuration from (2D) latent space boundaries.

    :param data: Dataframe with latent points (latent_0 and latent_1).
    :return: list of tuples with the latent limits, for each axis:
            [(min_0, max_0), (min_1, max_1)
    """
    return core.get_latent_limits(data)


@st.cache_data(show_spinner='Computing grid...')
def create_latent_grid_edges(grid_config):
    """Create the latent grid edges from a (2D) latent grid config,
    as the grid subspaces boundaries

    :param grid_config: [(min_0, max_0, steps_0), (min_1, max_1, steps_1)
    :return: (bin_edges_0, bin_edges_1), tuple of arrays with the equispaced
        bin edges for both axes.
    """
    return core.create_latent_grid_edges(grid_config)


@st.cache_data(show_spinner="Getting data split...")
def filter_data_split(dataset_config, _dataset, data, data_split=None):
    """Filter inputs and labels for a given data split, and join to data

    :param dataset_config: Dataset configuration (for caching purposes).
        Used to evaluate streamlit cache hit.
    :param _dataset: Dataset instance.
        Ignored in the streamlit cache hit evaluation.
    :param data: Dataframe to join to inputs and labels.
    :param data_split: Data split to filter, from 'train', 'test',
        'val' or 'all', defaults to None, as 'all'.
    :return: Data, joined to corresponding filtered inputs and labels.
    """
    logging.info("Getting data split...")
    # Filter data.
    inputs = _dataset.get_inputs_split(data_split)
    labels = _dataset.get_labels_split(data_split)
    data = data.join(inputs).join(labels)

    return data


@st.cache_data(show_spinner='Computing means...')
def compute_means(model_path, _model, data, _scaler):
    """Compute means of data on each bin of the latent grid, and
    the generated reconstructions of the latent points means.

    :param model_path: Path to model folder.
        Used to evaluate streamlit cache hit.
    :param _model: Keras model to use for reconstructions of latent means.
        Ignored in the streamlit cache hit evaluation.
    :param data: Dataframe with assigned latent bins (latent_bin_0 and
        latent_bin_1).
    :param _scaler: Scaler to inverse transform the reconstructions.
        Ignored in the streamlit cache hit evaluation.
    :return: Means dataframe, with latent_bin_0, latent_bin_1 as main indexes
        levels, with other secondary indexes, except the sample index.
    """
    return core.compute_means(_model, data, _scaler)


@st.cache_data(show_spinner=False)
def assign_latent_bins_from_edges(latent, bin_edges_0, bin_edges_1):
    """Assign the samples latent points to a bin in the grid.

    :param latent: Dataframe with the latent space points per sample,
        latent_0 and latent_1 (optionally with more index levels).
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :return: The assigned latent grid bins, and count of samples per bin.
    """
    return core.assign_latent_bins_from_edges(latent, bin_edges_0, bin_edges_1)


@st.cache_data(show_spinner='Generating equispaced sampling mean...')
def generate_equispaced_samples_mean(model_path, _model, _index,
                                     bin_edges_0, bin_edges_1,
                                     sampling_granularity, _scaler):
    """Generate equispaced latent points sampled on each latent grid,
    compute their reconstructions and the mean of this reconstructions.

    :param model_path: Path to model folder.
        Used to evaluate streamlit cache hit.
    :param _model: Keras model to use for reconstructions of latent points.
        Ignored in the streamlit cache hit evaluation.
    :param _index: pd.Index with the expected indexes for a sample.
        Ignored in the streamlit cache hit evaluation.
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :param sampling_granularity: Granularity of sampling per bin and per axis,
        in the latent space.
        The number of samples per bin will be then sampling_granularity ^ 2.
    :param _scaler: Scaler to inverse transform the reconstructions.
        Ignored in the streamlit cache hit evaluation.
    :return: Mean of equispaced latent samples resconstructions.
    """
    return core.generate_equispaced_samples_mean(_model, _index,
                                                 bin_edges_0, bin_edges_1,
                                                 sampling_granularity, _scaler)


@st.cache_data(show_spinner='Getting data from split...')
def get_scaled_data_split(dataset_config, _dataset, data_split):
    """Get Scaled data from selected split.

    :param dataset_config: Dataset configuration (for caching purposes).
        Used to evaluate streamlit cache hit.
    :param _dataset: Dataset instance.
        Ignored in the streamlit cache hit evaluation.
    :param data_split: Selected split, defaults to None, as all data.
    :return: Selected data.
    """
    return _dataset.get_scaled_data_split(data_split)


@st.cache_data(show_spinner='Getting available anomaly levels...')
def get_available_anomaly_levels(dataset_config, _dataset):
    """Get available anomaly levels from the dataset expected shape,
    sample level, sample_step level, sample_pixel level.

    :param dataset_config: Dataset configuration (for caching purposes).
        Used to evaluate streamlit cache hit.
    :param _dataset: Dataset instance.
        Ignored in the streamlit cache hit evaluation.
    :return: List of levels.
    """
    return core.get_available_anomaly_levels(_dataset)


@st.cache_data(show_spinner='Getting threshold config...')
def get_default_threshold_config(
        dataset_config, _dataset, data, th_level, th_std_factor=3):
    """Get the default threshold config, from the selected anomaly level score,
    as the mean of anomaly score + the factored std deviation of the dataset
    computed results.

    :param dataset_config: Dataset configuration (for caching purposes).
        Used to evaluate streamlit cache hit.
    :param _dataset: Dataset instance.
        Ignored in the streamlit cache hit evaluation.
    :param data: Dataframe with the anomaly score to threshold.
    :param th_level: Chosen anomaly level (sample, sample_step, sample_pixel).
    :param th_std_factor: Factor for std deviation, defaults to 3
    :return: anomaly_score_metric: the name of the anomaly score metric used
        (chosen given the th_level)
            max_threshold: Maximum value of anomaly_score_metric.
            default_threshold: mean(anomaly_score_metric) +
                th_std_factor * std(anomaly_score_metric)
    """
    return core.get_default_threshold_config(
        _dataset, data, th_level, th_std_factor)


@st.cache_data(show_spinner='Detecting anomalies...')
def detect_anomalies(samples, anomaly_score_metric, threshold):
    """Classify samples as anomalous or not, by filtering by thresholds

    :param samples: Dataframe of samples to classify.
    :param anomaly_score_metric: Anomaly score metric to threshold.
    :param threshold: Threshold value.
    :return: samples, with a new "anomalous" column with True/False.
    """
    return core.detect_anomalies(samples, anomaly_score_metric, threshold)


@st.cache_data(show_spinner='Computing anomalies statistics...')
def compute_anomalies_stats(latent, anomaly_score_metric):
    """Compute anomalies statistics

    :param samples: Dataframe of samples.
    :param anomaly_score_metric: Chosen anomaly score metric
        (sample, sample_step, sample_pixel).
    :return: dict with statistics.
    """
    return core.compute_anomalies_stats(latent, anomaly_score_metric)


@st.cache_data(show_spinner='Plotting ROC Curve...')
def plot_roc(roc, title):
    """Plot ROC with FPR vs TPR, with threshold in tooltip.

    :param roc: List of arrays, [fpr, tpr, thresholds]
    :return: ROC figure.
    """
    fig = plotter.plot_roc(roc)
    fig.update_layout(
        title=dict(text=title)
    )
    return fig


@st.cache_data(show_spinner='Plotting histogram...')
def plot_histogram(df, column, threshold, max_error, title):
    """Plot histogram for a given column in dataframe, adding a vertical line.
    The histogram is colored by "class" column of dataframe.

    :param df: Dataframe with the column to be used for the histogram
        computation and the "class" column to be used for colors.
    :param column: Column to be plotted in the histogram.
    :param threshold: Value where vertical line is placed.
    :param max_error: Maximum value to plot in the histogram horizontal axis.
        Values beyond are clipped.
    :param title: String for the figure title.
    :return: Histogram figure.
    """
    fig = plotter.plot_histogram(df, column, threshold, max_error)
    fig.update_layout(title_text=title)
    return fig


@st.cache_data(show_spinner=False)
def plot_loss(history):
    """Plot history loss.

    :param history: Dict with history with training metrics as keys.
    :return: Plot fig.
    """
    fig = plotter.plot_loss(history)
    return fig


@st.cache_data(show_spinner='Plotting Latent Space...')
def plot_latent_space(latent,
                      anomaly_score_metric,
                      max_color_value,
                      bin_edges_0,
                      bin_edges_1,
                      divide_by_group=False):
    """Plot (2D) latent space, colored by the anomaly metric,
    with different symbols (and sizes) for normal and abnormal data (based on
    detection), and optionally grouped by the group field.


    :param latent: Dataframe with the latent information per sample.
    :param anomaly_score_metric: Anomaly score metric name to use as color.
    :param max_color_value: Max value to set in the coloraxis.
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :param divide_by_group: Create traces by group field, defaults to False.
    :return: Latent space figure.
    """
    fig = plotter.plot_latent_space(latent, anomaly_score_metric,
                                    max_color_value,
                                    divide_by_group=divide_by_group)
    return plotter.add_grid_to_scatter(fig, bin_edges_0, bin_edges_1)


@st.cache_data(show_spinner='Plotting Means in Latent Grid...')
def plot_latent_grid_means(means, samples, column, cache_hash=None, _fig=None):
    """Plot latent grid with means, and optionally anomalies and
    reconstructions, and heatmaps, for a given feature column.

    :param means: Means dataframe.
    :param column: Feature column name.
    :param _fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    fig = plotter.plot_latent_grid_means(means, column, fig=_fig)
    return plotter.add_samples_annotations(samples, fig)


@st.cache_data(show_spinner='Plotting Heatmaps in Latent Grid...')
def plot_latent_grid_heatmap(samples, column, anomalies_only, legend_prefix,
                             cache_hash=None, _fig=None):
    """Plot latent grid with samples heatmaps, for a given feature column.

    :param samples: Samples dataframe (with inputs and reconstructions),
    :param column: Feature column name.
    :param anomalies_only: Whether to plot only anomalies heatmaps.
    :param legend_prefix: Prefix to prepend in legend entry.
    :param _fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    return plotter.plot_latent_grid_heatmap(
        samples, column, anomalies_only=anomalies_only,
        legend_prefix=legend_prefix, fig=_fig)


@st.cache_data(show_spinner='Plotting Samples in Latent Grid...')
def plot_latent_grid_samples(samples, column, anomalies_only, legend_prefix,
                             cache_hash=None, _fig=None):
    """Plot latent grid with samples and reconstructions, for a given
    feature column.

    :param samples: Samples dataframe (with inputs and reconstructions).
    :param column: Feature column name.
    :param anomalies_only: Whether to plot only anomalies.
    :param legend_prefix: Prefix to prepend in legend entry.
    :param _fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    return plotter.plot_latent_grid_samples(
        samples, column, anomalies_only=anomalies_only,
        legend_prefix=legend_prefix, fig=_fig)


@st.cache_data(show_spinner='Plotting Latent Grid...')
def plot_latent_grid_2d(means, samples, columns, cache_hash=None, _fig=None):
    """Plot latent grid with means, for given feature columns.

    :param means: Means dataframe.
    :param columns: List of feature columns names.
    :param samples: Samples dataframe, to create annotations of number of
        anomalies/samples per bin, defaults to None
    :param _fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    fig = plotter.plot_latent_grid_2d(means, columns, fig=_fig)
    return plotter.add_samples_annotations(samples, fig)


@st.cache_data(show_spinner='Plotting sample...')
def plot_sample(samples, column):
    """Plot sample sequences for a given feature column.

    :param samples: Samples dataframe (typically with one sample).
    :param column: Feature column name.
    :return: Sample figure.
    """
    return plotter.plot_sample(samples, column)


@st.cache_data(show_spinner='Plotting sample...')
def plot_sample_2d(samples, columns):
    """Plot a 2D sample for given feature channels columns.

    :param samples: Samples dataframe (typically with one sample).
    :param column: Feature column name.
    :return: Sample figure.
    """
    return plotter.plot_sample_2d(samples, columns)
