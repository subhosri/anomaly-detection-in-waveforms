import plotly.express as px
import streamlit as st

import app.core as app_core
import app.streamlit_utils as st_utils
import app.ui as ui
import core.data as data


def main():

    st.markdown(
        """
        <style>
            .stButton button {
                display: block;
                margin: auto;
            }
        </style>
        """, unsafe_allow_html=True
    )

    sidebar_title_ph = st.sidebar.empty()
    sidebar_title_ph.write('# Dataset Exploration')

    display_options = st.sidebar.expander(
        'Display chart options', expanded=True)

    col1, col2 = display_options.columns(2)
    dist_config = col1.radio('Distribution by:',
                             options=['class', 'group',
                                      'class-group', 'group-class'],
                             key='orig_dist_config')

    orientation = col2.radio('Orientation:',
                             options=['vertical', 'horizontal'],
                             key='orig_orient')[0]

    ui.add_clear_cache_ui()

    tabs_names = ['Select dataset', ' Distribution', 'Save dataset']
    tabs = st.tabs(tabs_names)

    # --------------------------------------------------------------- #
    # TAB 0: Dataset information panel.
    # dataset_selector = st.sidebar.form('dataset_selector')
    dataset_selector = st.sidebar.container()
    dataset_name = dataset_selector.selectbox(
        'Dataset', options=['Create a dataset'] + data.get_known_datasets(),
        key='selected_dataset')
    # submit_dataset = dataset_selector.form_submit_button('Run')

    if dataset_name == "Create a dataset":
        ds_creator = tabs[0].form('dataset_creator')

        file = ds_creator.file_uploader(
            label='Load a file',
            type='csv',
            accept_multiple_files=False,
            key='file_uploader',
            help='Maximum 200MB')
        dataset_name = ds_creator.text_input(
            'Dataset Name', value='NewDataset', key='name')
        data_path = ds_creator.text_input(
            'Data Path', value='NewDataset', key='data_path')
        load_2d = ds_creator.checkbox('Is 2D format', key='load_2d')

        ds_selector_cols = ds_creator.columns(10)
        create_ds = ds_selector_cols[0].form_submit_button('Create')

        if not create_ds:
            st.stop()
        else:
            # Save file in disk.
            # Create ds config file.
            st.stop()
    else:
        sidebar_title_ph.empty().write(f"# {dataset_name} Dataset Exploration")
        ds, ds_config = app_core.get_dataset(dataset_name)

    # Dataset info panel UI.
    ds_info = tabs[0].container()

    # Show info.
    ds_info.subheader(f'Available Columns: {ds.columns}')
    ds_info.subheader('Dataset Config:')
    ds_info.json(ds_config)

    # --------------------------------------------------------------- #
    # TAB 1: Distributions panel UI.
    plot_orig, _, target_config, _, plot_target = tabs[1].columns(
        [10, 0.5, 6, 0.5, 10])
    plot_orig.header('Original Distribution')
    target_config.header('Target configuration')
    plot_target.header('Target Distribution')

    # Plot Original distribution
    labels_dist = get_dist(ds_config, ds, ds.labels)
    fig = plot_distribution(labels_dist, dist_config, orientation)
    plot_orig.plotly_chart(fig, use_container_width=True)

    # Edit target.
    target_fields = ['class', 'group']
    selected_label_col_index = 0
    label_col = target_fields[selected_label_col_index]

    target_dist_dict = None
    if ds_config['prepare_data']['target_distrib'] is not None:
        label_col, target_dist_dict = \
            ds_config['prepare_data']['target_distrib']
        selected_label_col_index = target_fields.index(label_col)

    selected_label_col = target_config.radio('Target field:',
                                             options=target_fields,
                                             index=selected_label_col_index,
                                             horizontal=True)

    original_dist = get_dist(ds_config, ds, ds.labels,
                             label_col=selected_label_col, normalize=True)
    original_dist = original_dist.to_frame().reset_index()
    original_dist.columns = ['category', 'proportion']
    target_dist = original_dist

    if selected_label_col == label_col and target_dist_dict is not None:
        target_dist['proportion'] = target_dist['category'].map(
            {k: v for k, v in zip(
                list(target_dist_dict.keys()),
                [float(value) for value in target_dist_dict.values()])
             }
        )
        target_config.info('Showing loaded dataset target distribution!')

    with target_config.form("data_editor_form"):
        st.caption("Edit the target distribution here")
        edited = st.experimental_data_editor(target_dist,
                                             use_container_width=True,
                                             num_rows='dynamic')
        submit_button = st.form_submit_button(
            ":arrow_forward: Apply target distribution :arrow_forward:")

    # Validate target config.
    if submit_button:
        edited = edited.dropna()
        if edited.empty:
            target_config.warning('Target distribution is empty!')
            edited = None
        else:
            valid_cats = ds.labels[
                selected_label_col].dtype.categories.to_list()
            invalid_cats = validate_target_dist_keys(edited, valid_cats)
            if invalid_cats:
                target_config.error(
                    'Target distribution has invalid categories! '
                    f'{" ".join(invalid_cats)}')
                edited = None
            elif not validate_target_dist_sum_values(edited):
                target_config.error(
                    'Target distribution sum must be lower or equal 1.0!')
                edited = None

    # Get target distribution.
    if edited is not None and not edited.equals(original_dist):
        edited = {k: v for k, v in edited.to_dict(orient='split')['data']}
        final_target_distrib = (selected_label_col, edited)
        enable_save = True

        target_labels_index = get_labels_target_distrib(
            ds_config, ds, final_target_distrib)
        target_labels = ds.labels.loc[target_labels_index]
    else:
        enable_save = False
        target_labels = ds.labels

    # Plot target distribution.
    target_labels_dist = get_dist(ds_config, ds, target_labels)
    fig = plot_distribution(target_labels_dist, dist_config, orientation)
    plot_target.plotly_chart(fig, use_container_width=True)

    # --------------------------------------------------------------- #
    # TAB 2: Save panel UI
    ds_save = tabs[2].container()

    # Save new distribution.
    with ds_save:
        new_file_name = st.text_input(
            'New dataset name:', value=ds.name)
        save_overwrite = st.checkbox(
            'Overwrite if exists', key='save_overwrite')
        dataset_config_file = (data.DATASETS_PATH /
                               new_file_name).with_suffix('.json')
        disable_save_button = False
        if not save_overwrite and dataset_config_file.exists():
            ds_save.error('A dataset with same name already exists.')
            disable_save_button = True
        save = st.button("Save", disabled=disable_save_button)

    if save:
        if enable_save:
            ds.target_distrib = final_target_distrib
            ds.save_config(save_overwrite)


def validate_target_dist_keys(target_distrib, valid_cats):
    return [k for k in target_distrib['category'] if k not in valid_cats]


def validate_target_dist_sum_values(target_distrib):
    total = target_distrib['proportion'].sum()
    if total > 1.0:
        return False
    return True


@st.cache_data(show_spinner='Getting label distribution...')
def get_dist(
        dataset_config, _dataset, labels, label_col=None, normalize=False):
    return _dataset.get_dist(labels, label_col=label_col, normalize=normalize)


@st.cache_data(show_spinner='Getting target distribution...')
def get_labels_target_distrib(
        dataset_config, _dataset, target_distrib):
    return _dataset.get_labels_target_distrib(target_distrib)


@st.cache_data(show_spinner='Plotting distribution...')
def plot_distribution(labels_dist, dist_config, orientation):
    print('Plotting distribution...')
    labels_dist = labels_dist.to_frame().rename(
        columns={0: 'sample_count'}).reset_index()

    dist_config = dist_config.split('-')
    labels_dist = labels_dist.groupby(
        dist_config, sort=False)['sample_count'].sum().reset_index()

    label_col = dist_config[0]
    color = None
    if len(dist_config) > 1:
        color = dist_config[1]

    if orientation == 'v':
        x, y = label_col, 'sample_count'
        layout_def = {'xaxis_type': 'category'}
    else:
        y, x = label_col, 'sample_count'
        layout_def = {'yaxis_type': 'category'}

    fig = px.bar(labels_dist, x=x, y=y, color=color,
                 orientation=orientation, text_auto=True)
    fig.update_layout(layout_def)
    fig.update_layout(
        template='simple_white',
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )

    return fig


if __name__ == "__main__":
    st.set_page_config(page_title='Datasets', layout='wide')
    st_utils.set_page_container_style()
    st_utils.inject_background_style()
    main()
