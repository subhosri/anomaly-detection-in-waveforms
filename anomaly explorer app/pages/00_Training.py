import json
import re
from pathlib import Path

import streamlit as st
import tensorflow as tf
import toml

import app.core as app_core
import app.streamlit_utils as st_utils
import app.ui as ui
import common_models.autoencoder as ae
import core.app_core as compute_core
import core.data as data
import core.utils as utils

path_config = toml.load(Path().absolute() / 'path_config.toml')
SAVED_MODELS_PATH = Path(path_config["models"]["saved_models_path"])


def save_model(model_path, model, history):
    print(f'Saving model to {model_path}')
    model_path.mkdir(exist_ok=True)
    tf.keras.models.save_model(model, model_path, save_traces=False)
    utils.save_json(model_path / 'history.json', history.history)


def show_config_side_on_change():
    st.session_state.show_on_side = not st.session_state.show_on_side


def validate_hidden(text):
    try:
        list_integers = []
        if text:
            list_integers = list(map(int, re.split(r',', text)))
        return list_integers
    except Exception as e:
        st.error(
            'Layer filters/units must be a comma separated list of integers')
        st.error(e)


def add_containers_to_parent(parent_container, n_containers, as_columns=True):
    if as_columns:
        containers = parent_container.columns(n_containers)
    else:
        containers = [parent_container.container()
                      for i in range(n_containers)]
    return containers


def main():
    # TODO: Separate UI from running process.
    print('Running train main!!')

    with st.sidebar:
        title_placeholder = st.empty()
        title_placeholder.write('# Model Training')
        show_config_placeholder = st.empty()

    if 'show_on_side' not in st.session_state:
        st.session_state.show_on_side = False

    show_on_side = st.session_state.show_on_side

    if show_on_side:
        show_config_placeholder.button("Show config on main window",
                                       on_click=show_config_side_on_change,
                                       key='show_on_window_check')

        controls_container = st.sidebar
        st.markdown("""
                        <style>
                            .streamlit-expanderHeader {
                                font-size: medium;
                                font-weight: bold;
                            }
                        </style>
                    """,
                    unsafe_allow_html=True)

    else:
        show_config_placeholder.empty()
        show_config_placeholder.button("Show config on sidebar",
                                       on_click=show_config_side_on_change,
                                       key='show_on_side_check')
        controls_container = st.container()
        st.markdown("""
                        <style>
                            .streamlit-expanderHeader {
                                font-size: x-large;
                                font-weight: bold;
                            }
                        </style>
                    """,
                    unsafe_allow_html=True)

    # ----- Dataset controls -----------
    with controls_container:
        dataset_expand = st.expander(
            "Dataset selection", expanded=not show_on_side)
        dataset_config = add_containers_to_parent(
            dataset_expand, 2, as_columns=not show_on_side)

    dataset = dataset_config[0].selectbox(
        'Dataset', ["Select dataset"] + data.get_known_datasets(),
        key='dataset')

    if dataset == "Select dataset":
        st.stop()

    title_placeholder.write(f'# {dataset} Model Training')

    # Load dataset:
    ds, ds_config = app_core.get_dataset(dataset)

    all = dataset_config[0].checkbox("Select all")
    if all:
        default = ds.columns
    else:
        default = ds.columns[0]
    selected_columns = dataset_config[1].multiselect(
        'Dataset columns',
        options=ds.columns,
        default=default)

    # ----- Config controls -----------
    with controls_container:
        model_path_expand = st.expander(
            "Model path", expanded=not show_on_side)
        model_path_config = add_containers_to_parent(
            model_path_expand, 3, as_columns=not show_on_side)

        model_params_expand = st.expander(
            "Model parameters", expanded=not show_on_side)
        model_params_config_row_0 = add_containers_to_parent(
            model_params_expand, 5, as_columns=not show_on_side)
        model_params_config_row_1 = add_containers_to_parent(
            model_params_expand, 5, as_columns=not show_on_side)

        training_params_expand = st.expander(
            "Training parameters", expanded=not show_on_side)
        training_params_config = add_containers_to_parent(
            training_params_expand, 3, as_columns=not show_on_side)
        optimizer_config = add_containers_to_parent(
            training_params_expand, 4, as_columns=not show_on_side)
        thres_expander = st.expander("Thresholding controls",
                                     expanded=not show_on_side)
        container = st.columns(2)
        button_placeholder = container[0].empty()
        spinner_placeholder = container[1].container()

    autoencoder_model = model_params_config_row_0[0].selectbox(
        'Autoencoder Model',
        ('Basic', 'Variational', 'Adversarial', 'Diffusion'),
        key="autoencoder_model")
    conv_filters = model_params_config_row_0[1].text_input(
        'Convolutional layers filters list',
        value="",
        key='conv_filters')
    conv_filters = validate_hidden(conv_filters)
    hidden_layers = model_params_config_row_0[2].text_input(
        'Hidden layer units list',
        value="60, 30, 15, 6",
        key='hidden_layers')
    hidden_layers = validate_hidden(hidden_layers)
    latent_dim = model_params_config_row_0[3].number_input(
        "Latent dimensions",
        min_value=1,
        value=2,
        help="Select latent dimensions.",
        key='latent_dim')
    add_bn = model_params_config_row_0[4].checkbox(
        'Add Batch Norm layers', key='add_bn')
    hidden_activation = model_params_config_row_1[0].selectbox(
        'Hidden activation function', ('leakyrelu', 'relu', 'swish'),
        key='hidden_activation')
    output_activation = model_params_config_row_1[1].selectbox(
        'Output activation function',
        ('sigmoid', 'linear', 'tanh'),
        key='output_activation')
    dropout = model_params_config_row_1[2].number_input(
        "Dropout",
        min_value=0.0,
        max_value=0.5,
        step=0.01,
        value=0.0,
        format="%.2f",
        help="Select dropout rate.",
        key='dropout')
    beta_enabled = 'Variational' in autoencoder_model \
                   or "Adversarial" in autoencoder_model
    beta = model_params_config_row_1[3].number_input(
        "Beta",
        min_value=0.0,
        max_value=1.0,
        step=0.01,
        value=1.0,
        format="%.2f",
        help='Select beta value.',
        key='beta',
        disabled=not beta_enabled)
    reconstruction_loss = model_params_config_row_1[4].selectbox(
        'Reconstruction Loss',
        ('mean_squared_error', 'binary_crossentropy', 'mean_absolute_error'),
        key='reconstruction_loss')
    model_path_config[0].text_input(
        'Save model to this folder',
        value='models',
        key='model_base_path')
    model_path_config[1].text_input(
        'Model name',
        value=f'{dataset}_{autoencoder_model}',
        key='model_name')
    save_overwrite = model_path_config[2].checkbox(
        'Overwrite if exists', key='save_overwrite')

    # Training parameters
    batch_size = training_params_config[0].number_input(
        "Batch size",
        min_value=32,
        value=512,
        step=32,
        help="Select training Batch size.",
        key='batch_size')
    epochs = training_params_config[1].number_input(
        "Epochs",
        min_value=1,
        value=30,
        help="Select training epochs.",
        key='epochs')
    optimizer = training_params_config[2].selectbox(
        'Optimizer',
        ('Adam', 'SGD', 'RMSprop'),
        key='optimizer')
    training_params = {
        "optimizer": optimizer,
        "batch_size": batch_size,
        "epochs": epochs,
    }

    # Specific Optimizer configuration.
    learning_rate = optimizer_config[0].number_input(
        "Learning Rate",
        min_value=0.0001,
        value=0.0005,
        step=0.0001,
        format="%.4f",
        help="Select Learning Rate.",
        key='learning_rate')

    if optimizer == 'Adam':
        beta_1 = optimizer_config[1].number_input(
            "Adam Beta 1",
            min_value=0.8,
            value=0.9,
            step=0.01,
            format='%.2f',
            help="Select exponential decay rate for the 1st moment estimates.",
            key='beta_1')

        beta_2 = optimizer_config[2].number_input(
            "Adam Beta 2",
            min_value=0.9,
            value=0.999,
            step=0.001,
            format='%.3f',
            help="Select exponential decay rate for the 2nd moment estimates.",
            key='beta_2')

        optimizer_config[3].write("# ")
        amsgrad = optimizer_config[3].checkbox(
            'Apply AMSGrad variant', key='amsgrad')

        optimizer_config = {
            "learning_rate": learning_rate,
            "beta_1": beta_1,
            "beta_2": beta_2,
            "amsgrad": amsgrad
        }

        optimizer_instance = tf.keras.optimizers.Adam(**optimizer_config)

    elif optimizer == 'SGD':
        momentum = optimizer_config[1].number_input(
            "SGD momentum",
            min_value=0.0,
            value=0.0,
            step=0.01,
            max_value=1.0,
            format='%.2f',
            help="Select SGD momentum.",
            key='sgd_momentum')

        optimizer_config[2].write("# ")
        nesterov = optimizer_config[2].checkbox(
            'Apply Nesterov momentum', key='nesterov')

        optimizer_config = {
            "learning_rate": learning_rate,
            "momentum": momentum,
            "nesterov": nesterov
        }

        optimizer_instance = tf.keras.optimizers.SGD(**optimizer_config)

    elif optimizer == 'RMSprop':
        rho = optimizer_config[1].number_input(
            "Rho",
            min_value=0.8,
            value=0.9,
            step=0.01,
            format='%.2f',
            help="Select discounting factor for the history/coming gradient",
            key='rho')
        momentum = optimizer_config[2].number_input(
            "RMSprop momentum",
            min_value=0.0,
            value=0.0,
            step=0.01,
            format='%.2f',
            max_value=1.0,
            help="Select RMSprop momentum.",
            key='rmsprop_momentum')

        optimizer_config[3].write("# ")
        centered = optimizer_config[3].checkbox(
            'Centered',
            help=""" If True, gradients are normalized by the estimated
            variance of the gradient;
            if False, by the uncentered second moment.""",
            key='centered')

        optimizer_config = {
            "learning_rate": learning_rate,
            "rho": rho,
            "momentum": momentum,
            'centered': centered
        }

        optimizer_instance = tf.keras.optimizers.RMSprop(**optimizer_config)

    anomaly_metric = ui.append_threshold_criteria_to_placeholder(
        thres_expander)

    # Check the model name does not exist, to avoid overwriting models.
    model_path = SAVED_MODELS_PATH / \
        st.session_state.model_base_path / st.session_state.model_name
    if not save_overwrite and model_path.exists():
        model_path_expand.error('A model with same name already exists.')
        button_placeholder.empty()
        button_placeholder.button('Train', key='train_disabled', disabled=True)
        st.stop()

    button_placeholder.button('Train', key='train')
    # -------------------- Main window ---------------------
    plots_placeholder = st.empty()
    expander_logs = st.expander('Logs', expanded=False)
    logs_content = expander_logs.empty()
    summary_content = expander_logs.empty()

    if 'logs' in st.session_state:
        logs_content.code(st.session_state.logs)

    if 'summary' in st.session_state:
        summary_content.code(st.session_state.summary)

    if 'history' in st.session_state:
        plots_placeholder.plotly_chart(app_core.plot_loss(
            st.session_state.history.history),
            use_container_width=True)

    if st.session_state.train or ('retrain' in st.session_state
                                  and st.session_state.retrain):
        st.session_state.trained = False
        button_placeholder.empty()
        logs_content.empty()
        summary_content.empty()
        plots_placeholder.empty()

        with spinner_placeholder, st.spinner('Training model...'):

            def abort_training():
                logs_content.content += 'Training Aborted!'
                st.session_state.logs = logs_content.content
                st.session_state.summary = summary_content.content

            button_placeholder.button('Stop training', on_click=abort_training)

            # Right now, we only use the standard scaler if we select the DAE
            use_standardscaler = autoencoder_model == 'Diffusion'

            # Prepare data: Override default config
            ds_config['prepare_data']['columns'] = selected_columns
            ds_config['prepare_data']['standard_scaler'] = use_standardscaler
            ds = app_core.prepare_data(ds, **ds_config['prepare_data'])
            ds.save_scaler(model_path, save_overwrite)

            X_train_np = app_core.convert_to_numpy(ds.X_train)
            X_val_np = app_core.convert_to_numpy(ds.X_val)

            # X_test_np = app_core.convert_to_numpy(X_test)

            # Configs:
            model_config = {
                "hidden_dims": hidden_layers + [
                    latent_dim,
                ],
                "hidden_act": hidden_activation,
                "output_act": output_activation,
                "f_drop": dropout,
                "add_bn": add_bn,
                'conv_filters': conv_filters,
            }

            if beta_enabled:
                model_config.update({'f_beta': beta})

            # Logging Params:
            metadata = {
                'autoencoder_model': autoencoder_model,
                'dataset': dataset,
                'columns': selected_columns,
                'standard_scale': use_standardscaler,
                'reconstruction_loss': reconstruction_loss
            }

            training_config = {
                'model_config': model_config,
                'training_params': training_params,
                'optimizer_config': optimizer_config,
                "metadata": metadata
            }

            utils.save_json(
                model_path / 'training_config.json', training_config)

            # Initialise the desired model
            if autoencoder_model == "Basic":
                model = ae.Autoencoder(**model_config)
            elif autoencoder_model == "Variational":
                model = ae.VariationalAutoencoder(**model_config)
            elif autoencoder_model == "Adversarial":
                model = ae.AdversarialAutoencoder(**model_config)
            elif autoencoder_model == "Diffusion":
                model = ae.DiffusionAutoencoder(**model_config)
                model.m_enc.build(input_shape=X_train_np.shape)
            else:
                raise NotImplementedError(f"{autoencoder_model} not known")

            # If we use a linear output, we need to adapt the crossentropy.
            if output_activation == "linear" \
                    and reconstruction_loss == "binary_crossentropy":
                f_loss = tf.keras.losses.BinaryCrossentropy(
                    from_logits=True)
            elif output_activation != "linear" \
                    and reconstruction_loss == "binary_crossentropy":
                f_loss = tf.keras.losses.BinaryCrossentropy(
                    from_logits=False)
            else:
                f_loss = reconstruction_loss

            # Compile model:
            model.compile(optimizer=optimizer_instance, loss=f_loss)

            # cp_callback = tf.keras.callbacks.ModelCheckpoint(
            #     filepath=(SAVED_MODELS_PATH /
            #               st.session_state.model_base_path /
            #               st.session_state.model_name / 'checkpoints' /
            #               'ckpt-{epoch:04d}-{val_loss:.4f}'),
            #     save_weights_only=True,
            #     verbose=1)

            with st_utils.stdout(to=logs_content, format='code'), \
                    st_utils.stderr(to=logs_content, format='code'):

                print(f'Model config: {json.dumps(training_config, indent=4)}')

                history = model.fit(
                    x=X_train_np,
                    y=X_train_np,
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_data=(X_val_np, X_val_np),
                    validation_batch_size=1024,
                    # callbacks=[cp_callback],
                    verbose=2)
            st.session_state.logs = logs_content.content

            st.session_state.history = history
            st.session_state.model = model
            st.session_state.trained = True

            # Save relevant meta data
            model.add_meta(metadata)

            # Get default threshold and grid config from validation set.
            levels = app_core.get_available_anomaly_levels(
                ds.get_config(), ds)
            ds.results, anomaly_criteria = compute_core.compute_results(
                model, ds.X_val, ds.scaler, anomaly_metric)
            ds.results = compute_core.compute_anomaly_scores(
                ds.results, anomaly_criteria, levels)
            # Grid is based on latent space limits.
            # grid_config = [(min_0, max_0), (min_1, max_1)]
            grid_config = compute_core.get_latent_limits(ds.results)

            # Get default thresholds from latent.
            # Per level, the threshold config is a tuple:
            # anomaly_score_metric, max_threshold, default_threshold
            thresholds = {}
            for level in levels:
                thresholds[level] = compute_core.get_default_threshold_config(
                    ds, ds.results, level, th_std_factor=3)

            evaluation = {
                "grid_config": grid_config,
                "thresholds": thresholds
            }
            utils.save_json(model_path / 'evaluation.json', evaluation)
            model.add_meta(evaluation)

            # And save the model
            save_model(model_path, model, history)

            with st_utils.stdout(to=summary_content, format='code'), \
                    st_utils.stderr(to=summary_content, format='code'):
                print(model.summary(expand_nested=True))
                print(f'{json.dumps(evaluation, indent=4)}')
                print('Training Finished!')

            st.session_state.summary = summary_content.content

        # Remove stop button
        button_placeholder.empty()
        button_placeholder.button('Train', key='retrain')

        plots_placeholder.plotly_chart(app_core.plot_loss(history.history),
                                       use_container_width=True)


if __name__ == "__main__":
    st.set_page_config(page_title='Training app', layout='wide')
    st_utils.set_page_container_style()
    st_utils.inject_background_style()
    main()
