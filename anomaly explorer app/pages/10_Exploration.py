import streamlit as st

import app.core as app_core
import app.streamlit_utils as st_utils
import app.ui as ui
import common_models.autoencoder as ae
import core.utils as utils


def main():
    print('Running exploration main!!')
    (model_path, used_dataset, data_split_placeholder,
     thres_expander, grid_expander, sidebar_title_ph) = ui.render_controls_ui()

    # UI Content:
    analysis_tab, latent_tab, explorer_tab = st.tabs([
        "Analysis", "Latent Space", "Explore Anomalies",
    ])

    # Load (singleton) model from dropdown selection.
    model = app_core.load_model(model_path)

    # Load the respective configuration
    # If selected dasatet same as model training, split data
    split = False
    if used_dataset == "Model dataset":
        used_dataset = ae.get_meta(model, 'dataset')
        split = True
    used_features = ae.get_meta(model, 'columns')

    sidebar_title_ph.empty().write(f"# {used_dataset} Model Exploration")

    # Get data.
    # Scaler is loaded from model folder, in get_dataset args.
    # Then columns, standard_scaler, fit_on_train, fit_scaler_on not needed.
    ds, ds_config = app_core.get_dataset(used_dataset, scaler=model_path)
    ds_config['prepare_data']['split'] = split
    ds_config['prepare_data']['columns'] = used_features

    ds = app_core.prepare_data(ds, **ds_config['prepare_data'])

    data_split = None
    if ds.is_split:
        data_split = ui.append_split_select_to_placeholder(
            data_split_placeholder)
    data = app_core.get_scaled_data_split(ds_config, ds, data_split)

    anomaly_metric = ui.append_threshold_criteria_to_placeholder(
        thres_expander)

    # Select threshold, based on error stats, and default from latent.
    levels = app_core.get_available_anomaly_levels(ds_config, ds)
    th_level = ui.append_threshold_level_to_placeholder(thres_expander, levels)

    # Getting predictions (and latent encoding) for all!
    ds.results, anomaly_criteria = app_core.compute_results(
        model_path, model, data, ds.scaler, anomaly_metric)
    ds.results = app_core.compute_anomaly_scores(
        ds.results, anomaly_criteria, [th_level])

    # Get Default grid config from model metadata.
    # grid_config = [(min_0, max_0, steps_0), (min_1, max_1, steps_1)}
    evaluation_meta = utils.load_json(model_path / 'evaluation.json')
    grid_config_limits = app_core.get_latent_limits(ds.results)
    grid_config_values = evaluation_meta['grid_config']
    grid_config, sampling_granularity = ui.append_grid_limits_to_placeholder(
        grid_expander, grid_config_values, grid_config_limits)
    bin_edges_0, bin_edges_1 = app_core.create_latent_grid_edges(grid_config)

    # Compute the latent_bins for each sample, from latent.
    ds.results = app_core.assign_latent_bins_from_edges(
        ds.results, bin_edges_0, bin_edges_1)

    # Get Default Threshold config from model metadata.
    anomaly_score_metric, max_threshold, default_threshold = evaluation_meta[
        'thresholds'][th_level]
    threshold = ui.append_threshold_slider_to_placeholder(
        thres_expander, max_threshold, default_threshold)
    # Detect anomalies, based on threshold on anomaly_score_metric.
    ds.results = app_core.detect_anomalies(
        ds.results, anomaly_score_metric, threshold)

    # Generated the equispaced samples mean, from edges.
    equispaced_samples_mean = app_core.generate_equispaced_samples_mean(
        model_path, model, ds.results.index, bin_edges_0, bin_edges_1,
        sampling_granularity, ds.scaler)

    # Get selected split data, with inputs and labels from dataset.
    data_from_split = app_core.filter_data_split(
        ds_config, ds, ds.results, data_split)

    # Means on Selected split data, for later plotting.
    # Index level 0 is substituted by ['latent_bin_0', 'latent_bin_1']
    # keeping the rest of secondary indexes (sequence, height, width).
    means_from_split = app_core.compute_means(
        model_path, model, data_from_split, ds.scaler)
    means_from_split = means_from_split.join(
        equispaced_samples_mean, how='outer')

    # TODO: Re-Add custom samples for robustness study.
    # results = app_core.detect_anomalies(results, 'custom_set', th_metric,
    #                                     threshold)

    # UI
    with analysis_tab:
        ui.render_analysis_block_ui(
            data_from_split, anomaly_score_metric, threshold, max_threshold)
        ui.render_metadata_ui(model_path)

    with latent_tab:
        st.header(
            f'Latent Space - {ui.split_and_capitalize(anomaly_score_metric)}')
        divide_by_group = st.checkbox('Divide by group',
                                      value=False,
                                      key='latent_divide_by_group')
        ui.render_latent_space_ui(
            data_from_split, anomaly_score_metric, max_threshold,
            bin_edges_0, bin_edges_1, divide_by_group)
        ui.render_latent_grid_ui(
            data_from_split, means_from_split, used_features)

    with explorer_tab:
        st.header("Samples")
        cols = st.columns(3)
        divide_by_group = cols[0].checkbox(
            'Divide by Group', value=True, key='explorer_divide_by_group')
        only_anomalies = cols[1].checkbox(
            'Show only anomalies', value=True, key='explorer_only_anomalies')
        descending = cols[2].checkbox('Sort by descending anomaly metric',
                                      value=True, key='explorer_descending')
        ui.render_explorer_ui(data_from_split, means_from_split,
                              anomaly_score_metric, used_features,
                              divide_by_group, only_anomalies, descending)

    # TODO: Re-Add custom samples for robustness study.
    # with try_tab:
    #     st.header('Lets try some samples')
    #     render_latent_ui(results, 'custom_set', th_metric, threshold)
    print('Main has finished!!\n----------------------------------\n\n')


if __name__ == "__main__":
    st.set_page_config(page_title='Exploration', layout='wide')
    st_utils.set_page_container_style()
    st_utils.inject_background_style()
    main()
