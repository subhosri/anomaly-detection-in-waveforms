import logging
from pathlib import Path

import numpy as np
import streamlit as st
import toml

import app.core as app_core
import core.data as data

path_config = toml.load(Path().absolute() / 'path_config.toml')
SAVED_MODELS_PATH = Path(path_config["models"]["saved_models_path"])


def split_and_capitalize(string: str):
    """Format a snake_case string by splitting it into words
    and capitalize them.

    :param string: string to format.
    :return: The formatted string.
    """
    return ' '.join([word.capitalize() for word in string.split('_')])


def append_split_select_to_placeholder(container: st.container):
    """Append the data split selector to the placeholder container.

    :param container: the streamlit container where to put the control.
    :return: Selected data split.
    """
    data_split = container.radio("Data split",
                                 options=['test', 'train', 'val', 'all'],
                                 format_func=split_and_capitalize,
                                 help='Select the data split to process.',
                                 horizontal=True,
                                 label_visibility='collapsed')
    return data_split


def append_threshold_criteria_to_placeholder(
        container: st.container,
        options: list = ['MSE']):
    """Append the anomaly criteria control to the placeholder container.

    :param container: the streamlit container where to put the control.
    :param options: list of possible options
    :return: Selected anomaly criteria.
    """
    # TODO: Add more criteria
    anomaly_metric = container.radio(
        "Anomaly criteria",
        options=['MSE'],
        help='Select the anomaly criteria to threshold.',
        horizontal=True)
    return anomaly_metric


def append_threshold_level_to_placeholder(
        container: st.container,
        levels: list = ['sample', 'sample_step', 'sample_pixel']):
    """Append the anomaly level control to the placeholder container.

    :param container: the streamlit container where to put the control.
    :param levels: list of possible levels
    :return: Selected Threshold level value.
    """
    # Level of criteria: per sample, per sample and step, per sample and pixel
    th_level = container.radio(
        "Anomaly level",
        options=levels,
        format_func=split_and_capitalize,
        help='Select the anomaly level to threshold.',
        horizontal=True)
    return th_level


def append_threshold_slider_to_placeholder(container: st.container,
                                           max_threshold: float,
                                           default_threshold: float):
    """Append the threshold slider to the placeholder container.

    :param container: the streamlit container where to put the control.
    :param max_threshold: Maximum value for threshold.
    :param default_threshold: Default value for threshold.
    :return: Threshold slider value.
    """
    form = container.form("threshold_slider_form")
    threshold = form.slider(label="Anomaly Threshold",
                            min_value=float(0.0),
                            max_value=float(max_threshold),
                            value=float(default_threshold),
                            step=float(max_threshold / 50),
                            format='%.5f',
                            help="Select threshold.",
                            key='threshold')
    form.form_submit_button('Apply')
    return threshold


def append_grid_limits_to_placeholder(container: st.container,
                                      grid_config_values: list,
                                      grid_config_limits: list):
    """Append the grid control to the placeholder container.

    :param container: the streamlit container where to put the control.
    :param grid_config_values: list of tuples, with lower and upper values
        to create the grid, in each axis as [(min_x, max_x), (min_y, max_y).
        Typically from the model settings.
    :param grid_config_limits: list of tuples, with min and max limits
        to create the grid, in each axis as [(min_x, max_x), (min_y, max_y).
        Typically from the current dataset limits.
    :return:
        grid_config: updated grid config from user selection.
        sampling_granularity: number of samples per subspace in each direction
            for the equispaced sampling.
    """

    default_grid_config = [
        (min(grid_config_values[0][0], grid_config_limits[0][0]),
         max(grid_config_values[0][1], grid_config_limits[0][1])),
        (min(grid_config_values[1][0], grid_config_limits[1][0]),
         max(grid_config_values[1][1], grid_config_limits[1][1]))]

    grid_controls_form = container.form('Grid Controls Form')
    grid_limits_x = grid_controls_form.slider(
        label="Grid limits in X",
        min_value=default_grid_config[0][0],
        max_value=default_grid_config[0][1],
        value=(grid_config_values[0][0], grid_config_values[0][1]),
        step=float((default_grid_config[0][1] -
                   default_grid_config[0][0]) / 100),
        format='%.2f',
        help="Select grid limits in X.",
        key='grid_limits_x')
    grid_bin_x = grid_controls_form.slider(
        label="Number of grid bins in X",
        min_value=1,
        max_value=15,
        value=7,
        step=1,
        help="Select number of grid divisions in X axis.",
        key='grid_bin_x')
    grid_limits_y = grid_controls_form.slider(
        label="Grid limits in Y",
        min_value=default_grid_config[1][0],
        max_value=default_grid_config[1][1],
        value=(grid_config_values[1][0], grid_config_values[1][1]),
        step=float((default_grid_config[1][1] -
                   default_grid_config[1][0]) / 100),
        format='%.2f',
        help="Select grid limits in Y.",
        key='grid_limits_y')
    grid_bin_y = grid_controls_form.slider(
        label="Number of grid bins in Y",
        min_value=1,
        max_value=15,
        value=7,
        step=1,
        help="Select number of grid divisions in Y axis.",
        key='grid_bin_y')
    sampling_granularity = grid_controls_form.slider(
        label="Sampling granularity.",
        min_value=1,
        max_value=20,
        value=10,
        step=1,
        help="Select number of samples per subspace in each "
        "direction for the equispaced sampling.",
        key='sampling_granularity')
    grid_controls_form.form_submit_button("Apply")

    grid_config = [
        grid_limits_x + (grid_bin_x, ), grid_limits_y + (grid_bin_y, )
    ]

    return grid_config, sampling_granularity


def render_controls_ui():
    """Render the Exploration page control widgets, on the sidebar.

    :return:
        model_path: Selected model path to load.
        anomaly_metric: Selected thresholding anomaly criteria.
        data_split_placeholder: Selected dataset split.
        thres_expander: Thresholding controls container placeholder.
        grid_expander: Grid controls container placeholder.
        sidebar_title_ph: Sidebar title container placeholder.
    """
    sidebar_title_ph = st.sidebar.empty()
    sidebar_title_ph.write('# Model Exploration')

    models_paths = sorted(
        [path.relative_to(SAVED_MODELS_PATH)
         for model_base_path in SAVED_MODELS_PATH.iterdir()
         for path in model_base_path.iterdir()]
    )

    # Get model from dropdown selection.
    model_expander = st.sidebar.expander("Model selection",
                                         expanded=True)
    model_form = model_expander.form('model_selection')
    model_path = model_form.selectbox('Select a model',
                                      options=['Select a model'] +
                                      models_paths,
                                      key='model_path')
    selected_dataset = model_form.selectbox('Select a dataset',
                                            options=['Model dataset'] +
                                            data.get_known_datasets(),
                                            key='selected_dataset')
    model_form.form_submit_button("Run")

    # Configuration controls
    data_split_placeholder = st.sidebar.expander("Data split",
                                                 expanded=True)

    thres_expander = st.sidebar.expander("Thresholding Controls",
                                         expanded=True)
    grid_expander = st.sidebar.expander("Grid Controls", expanded=False)

    if model_path == 'Select a model':
        st.stop()
    model_path = SAVED_MODELS_PATH / model_path

    clear_container = add_clear_cache_ui()
    clear_container = add_clear_figs_ui(container=clear_container)

    return (model_path, selected_dataset, data_split_placeholder,
            thres_expander, grid_expander, sidebar_title_ph)


def add_clear_cache_ui(container=None):
    if container is None:
        container = st.sidebar.expander("Clear", expanded=False)
    clear_cache = container.button("Clear cache")
    if clear_cache:
        st.cache_data.clear()
        st.cache_resource.clear()

    return container


def add_clear_figs_ui(container=None):
    if container is None:
        container = st.sidebar.expander("Clear", expanded=False)

    # Clearing ui
    clear_figs_cache = container.button("Clear figures")
    if clear_figs_cache:
        render_analysis_block_ui.clear()
        app_core.plot_histogram.clear()
        app_core.plot_roc.clear()

        render_metadata_ui.clear()
        app_core.plot_loss.clear()

        render_latent_space_ui.clear()
        app_core.plot_latent_space.clear()

        app_core.plot_latent_grid_means.clear()
        app_core.plot_latent_grid_heatmap.clear()
        app_core.plot_latent_grid_samples.clear()

        app_core.plot_sample.clear()
        app_core.plot_latent_grid_2d.clear()
        app_core.plot_sample_2d.clear()

    # TODO: This a streamlit bug: Clearing the session state
    # does not re-render the UI widgets with the default values,
    # although the widgets are "new"!.
    # Also, widget state is lost when switching pages!
    # reset_session = cache2.button("Reset session")
    # if reset_session:
    #     # Delete all the items in Session state
    #     for key in st.session_state.keys():
    #         del st.session_state[key]

    return container


@st.cache_data(show_spinner="Analysis of anomaly scores...")
def render_analysis_block_ui(
        data, anomaly_score_metric, threshold, max_threshold):
    """Render the analysis tab, with histograms and metrics.

    :param data: Dataframe with results.
    :param anomaly_score_metric: Chosen anomaly score metric
        (sample, sample_step, sample_pixel).
    :param threshold: Selected threshold value.
    :param max_threshold: Max threshold value
    """
    logging.info("Analysis of anomaly scores...")
    data = data[['class', 'anomalous', anomaly_score_metric]
                ].groupby(level=0).first()
    stats = app_core.compute_anomalies_stats(data, anomaly_score_metric)

    hist_fig = app_core.plot_histogram(
        data, anomaly_score_metric, threshold, max_threshold,
        split_and_capitalize(anomaly_score_metric))

    # Layout

    _, histogram, metrics, _ = st.columns(
        [0.01] + [3, 0.5] + [0.01], gap='medium')

    histogram.plotly_chart(hist_fig, use_container_width=True)

    metrics.metric("\# Samples", stats['num_samples'])  # noqa: W605
    metrics.metric(
        '\# Anomalies', stats['num_anomalies'],  # noqa: W605
        f"{stats['pct_anomalies']:.2%}", delta_color="off")

    metrics.metric(
        split_and_capitalize(f'mean_{anomaly_score_metric}'),
        np.format_float_scientific(stats[f'mean_{anomaly_score_metric}'],
                                   precision=2))
    metrics.metric(
        split_and_capitalize(f'std_dev_{anomaly_score_metric}'),
        np.format_float_scientific(stats[f'std_dev_{anomaly_score_metric}'],
                                   precision=2))
    metrics.metric(
        split_and_capitalize(f'max_{anomaly_score_metric}'),
        np.format_float_scientific(stats[f'max_{anomaly_score_metric}'],
                                   precision=2))

    if 'FP' in stats and 'FN' in stats:
        metrics = st.columns([0.01] + [1] * 4 + [0.01], gap='medium')
        metrics[1].metric(
            'False Positive Rate', f"{stats['FPR']:.2%}",
            f"{stats['FP']}", delta_color="off")
        metrics[2].metric(
            'True Positive Rate', f"{stats['TPR']:.2%}",
            f"{stats['TP']}", delta_color="off")
        metrics[3].metric(
            'True Negative Rate', f"{stats['TNR']:.2%}",
            f"{stats['TN']}", delta_color="off")
        metrics[4].metric(
            'False Negative Rate', f"{stats['FNR']:.2%}",
            f"{stats['FN']}", delta_color="off")

        _, roc, _, _ = st.columns([0.01] + [3, 0.5] + [0.01], gap='medium')
        roc_fig = app_core.plot_roc(stats['ROC'], 'ROC')
        roc.plotly_chart(roc_fig, use_container_width=True)


@st.cache_data(show_spinner="Model metadata...")
def render_metadata_ui(model_path):
    """Render the model metadata.

    :param model_path: Selected model path to load.
    """
    metadata, history = app_core.load_model_metadata(model_path)

    with st.expander('Model Training Metadata', expanded=False):
        st.write(metadata)

        if history is not None:
            st.plotly_chart(app_core.plot_loss(history),
                            use_container_width=True)


@st.cache_data(show_spinner="Loading Latent space...")
def render_latent_space_ui(data, anomaly_score_metric, max_threshold,
                           bin_edges_0, bin_edges_1, divide_by_group):
    """Render the latent space tab with latent space and latent grid plots.

    :param data: Dataframe with samples results.
    :param anomaly_score_metric: Chosen anomaly score metric
        (sample, sample_step, sample_pixel).
    :param max_threshold: Max threshold value
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :param divide_by_group: Whether to divide latent space points per group.
    """
    logging.info("Loading latent space...")
    anomaly_scores = sorted(
        [column for column in data.columns
         if column.endswith('anomaly_score')])
    latent_columns = ['latent_0', 'latent_1', 'latent_bin_0', 'latent_bin_1',
                      'class', 'group', 'anomalous'] + anomaly_scores
    latent = data[latent_columns].groupby(level=0).first()

    # Figure creation.
    latent_space = app_core.plot_latent_space(
        latent,
        anomaly_score_metric,
        max_threshold,
        bin_edges_0,
        bin_edges_1,
        divide_by_group=divide_by_group)

    st.plotly_chart(latent_space, use_container_width=True)


def render_latent_grid_ui(data, means, columns):
    """Render the latent space tab with latent space and latent grid plots.

    :param data: Dataframe with samples results.
    :param means: Dataframe with means per bin in the latent grid.
    :param columns: List of selected feature columns.
    """
    logging.info("Loading latent grid...")

    if data.index.nlevels == 2:
        has_anomalies = data['anomalous'].any()
        for column in columns:
            with st.expander(column):
                latent_grid_cont = st.container()
                col1, col2, col3 = st.columns(3)
                show_anomalies = col1.checkbox('Show Anomalies',
                                               value=has_anomalies,
                                               key='show_anomalies_' + column,
                                               disabled=not has_anomalies)
                show_heatmaps = col2.checkbox('Show Heatmaps',
                                              value=False,
                                              key='show_heatmaps_' + column)
                match_axis = col3.checkbox('Match Axis',
                                           value=True,
                                           key='match_axis_' + column)

                latent_grid = app_core.plot_latent_grid_means(
                    means, data, column)

                if show_anomalies:
                    latent_grid = app_core.plot_latent_grid_samples(
                        data, column, anomalies_only=True,
                        legend_prefix='Anomalies',
                        cache_hash=show_anomalies, _fig=latent_grid)

                if show_heatmaps:
                    latent_grid = app_core.plot_latent_grid_heatmap(
                        data, column, anomalies_only=False,
                        legend_prefix='Inputs',
                        cache_hash=(show_anomalies, show_heatmaps),
                        _fig=latent_grid)

                    if show_anomalies:
                        latent_grid = app_core.plot_latent_grid_heatmap(
                            data, column, anomalies_only=True,
                            legend_prefix='Anomalies',
                            cache_hash=show_anomalies, _fig=latent_grid)

                if not match_axis:
                    latent_grid.update_yaxes(matches=None)

                latent_grid_cont.plotly_chart(latent_grid,
                                              use_container_width=True)
    elif data.index.nlevels == 4:
        with st.expander('Latent grid'):
            latent_grid_cont = st.container()
            latent_grid = app_core.plot_latent_grid_2d(
                means,
                columns=columns,
                samples=data)
            latent_grid_cont.plotly_chart(latent_grid,
                                          use_container_width=True)


def render_explorer_ui(data, means, anomaly_score_metric, columns,
                       divide_by_group, only_anomalies, descending):
    """Render the sample explorer tab, with the sample plot and controls.

    :param data: Dataframe with samples results.
    :param means: Dataframe with means per bin in the latent grid.
    :param anomaly_score_metric: Chosen anomaly score metric
        (sample, sample_step, sample_pixel).
    :param columns: List of selected feature columns.
    :param divide_by_group: Whether to show a group selector.
    :param only_anomalies: Whether to show only anomalies in selector.
    :param descending: Whether to sort per anomaly score in descending order.
    """
    logging.info("Explore anomalies...")
    sample_selector = st.container()

    if only_anomalies:
        data = data.loc[data['anomalous']]

    if divide_by_group:
        groups = data['group'].unique()
        group_selector, sample_selector = sample_selector.columns([1, 2])
        group_selection = group_selector.selectbox(
            "Select group",
            label_visibility='collapsed',
            options=sorted(groups))
        data = data.loc[data['group'] == group_selection]

    sample_ids = data[[anomaly_score_metric]].groupby(level=0).first()
    options = sample_ids.sort_values(
        by=anomaly_score_metric,
        ascending=not descending).index.values.tolist()

    selection = sample_selector.selectbox("Select sample",
                                          label_visibility='collapsed',
                                          options=['Select sample'] + options)
    if selection != "Select sample":
        filtered_samples = data.loc[[selection]]
        equispaced_cols = [col + '_equispaced' for col in columns]
        mean_cols = [col + '_mean' for col in columns]

        filtered_samples = filtered_samples.join(
            means[mean_cols + equispaced_cols],
            on=['latent_bin_0', 'latent_bin_1'] +
            filtered_samples.index.names[1:],
            how='left')

        if len(filtered_samples) > 0:
            if filtered_samples.index.nlevels == 2:
                for column in columns:
                    with st.expander(column, expanded=True):
                        fig = app_core.plot_sample(filtered_samples, column)
                        st.plotly_chart(fig, use_container_width=True)
            elif filtered_samples.index.nlevels == 4:
                # Single channel or RGB, supported by px.imshow.
                # TODO: Support completely animations.
                # TODO: Support num channels not in (1, 3). Plot per channel.
                with st.expander(str(selection), expanded=True):
                    fig = app_core.plot_sample_2d(filtered_samples, columns)
                    st.plotly_chart(fig, use_container_width=True)
