import json
import logging
import pickle
from collections import defaultdict
from functools import partial
from multiprocessing import Pool
from pathlib import Path
from typing import Literal, Union

import numpy as np
import pandas as pd
import toml
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler

import core.utils as utils

path_config = toml.load(Path().absolute() / 'path_config.toml')
DATASETS_PATH = Path(path_config["data"]["datasets_path"])


class Dataset:

    def __init__(self, name, data_path=None, load_2d=None,
                 data=None, labels=None, scaler=None):
        # Check important arguments before continuing:
        if (data_path is None and data is None) or \
            (data_path is not None and load_2d is None) or \
                (data is not None and labels is None):
            raise ValueError(
                "To create a dataset from a csv file provide "
                "data_path and load_2d flag. "
                "Or you can provide directly data and labels dataframes.")
        self.name = name
        self.data_path = data_path
        self.load_2d: bool = load_2d

        # To be set in load_data implementations:
        self._data: pd.DataFrame = data
        self._labels: pd.DataFrame = labels

        # Scaler.
        self.scaler: Union[MinMaxScaler, StandardScaler] = None
        # How to filter data for scaling,
        # dict of columns with list of allowed values.
        self.fit_scaler_on: dict[list] = None
        self.scaled_data: pd.DataFrame = None

        # All columns.
        self.features_columns: list = None
        # Selected columns, will be set on prepare data,
        # or when setting the scaler, or set to all columns.
        self._columns: list = None

        # Splits.
        self.is_split = False
        self.train_labels: pd.DataFrame = None
        self.test_labels: pd.DataFrame = None
        self.val_labels: pd.DataFrame = None

        # Prepare data config:
        self.target_distrib: tuple = None
        self.split: bool = True
        self.test_size: float = 0.2
        self.val_random_state: int = 123
        self.standard_scaler: bool = False
        self.fit_on_train: bool = True
        self.fit_scaler_on: dict = {"class": ["normal"]}

        # Load data from dsv, if dataframes were not provided:
        if self._data is None:
            self.load_data()
        # All columns
        self.features_columns = self.data.columns.tolist()
        self._columns = self.features_columns
        if scaler is not None:
            # If path to provided, load scaler.
            if isinstance(scaler, (Path, str)):
                self.scaler = self.load_scaler(scaler)
            else:  # Assume valid scaler object.
                self.scaler = scaler
            self._columns = self.scaler.feature_names_in_

    @property
    def columns(self):
        return self._columns

    @columns.setter
    def columns(self, columns_list: list):
        if self.scaler is not None:
            logging.warning(
                'Setting the columns might invalidate the scaler! ' +
                'Fit the scaler or provide a new one if needed, ' +
                'to rescale the data.')
        self._columns = columns_list

    # Target data if requested, and not split, not scaled (inputs):
    @property
    def data(self):
        return self._data

    @property
    def labels(self):
        return self._labels

    @property
    def index(self):
        return self._data.index

    @property
    def shape(self):
        # Note: This does not support variable length at all.
        # To support it, we would need to change from fixed numpy arrays
        # to generators that are able to provide padded batches.
        return [
            len(index_level) for index_level in self._data.index.levels[1:]
        ] + [len(self.columns)]

    # Scaled data, target filtered if it was requested.
    @property
    def X(self):
        return self.scaled_data

    @property
    def X_train(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.scaled_data.query(
            f"{self.scaled_data.index.names[0]} in "
            f"{list(self.train_labels.index.values)}")

    @property
    def train_index(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.train_labels.index

    @property
    def X_test(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.scaled_data.query(
            f"{self.scaled_data.index.names[0]} in "
            f"{list(self.test_labels.index.values)}")

    @property
    def test_index(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.test_labels.index

    @property
    def X_val(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.scaled_data.query(f"{self.scaled_data.index.names[0]} in "
                                      f"{list(self.val_labels.index.values)}")

    @property
    def val_index(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.val_labels.index

    # As numpy arrays, (filtered if requested) scaled data.
    @property
    def X_np(self):
        return utils.convert_to_numpy(self.X)

    @property
    def X_train_np(self):
        return utils.convert_to_numpy(self.X_train)

    @property
    def X_val_np(self):
        return utils.convert_to_numpy(self.X_val)

    @property
    def X_test_np(self):
        return utils.convert_to_numpy(self.X_test)

    # TODO: y are all label columns, but not meant for the moment to be the y
    # of a model. We need to decide which would be the label column
    # (or create/encode it based on the user's criteria).
    # Note that there are no numpy repr of them.
    @property
    def y(self):
        return self.labels

    @property
    def y_train(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.train_labels

    @property
    def y_test(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.test_labels

    @property
    def y_val(self):
        if not self.is_split:
            raise ValueError('Data is not yet split.')
        return self.val_labels

    def get_scaled_data_split(
            self, split: Literal['train', 'test', 'val', 'all'] = None):
        """Get Scaled data from selected split.

        :param split: Selected split, defaults to None, as all data.
        :return: Selected data.
        """
        logging.info('Getting scaled data from split...')
        if split is None or split == 'all':
            data = self.X
        elif split == 'train':
            data = self.X_train
        elif split == 'test':
            data = self.X_test
        elif split == 'val':
            data = self.X_val
        return data

    def get_inputs_split(
            self, split: Literal['train', 'test', 'val', 'all'] = None):
        """Get original inputs and labels for a given data split.

        :param split: Selected split, defaults to None, as all data.
        :return: Selected inputs and labels.
        """
        logging.info('Getting inputs from split...')
        if split is None or split == 'all':
            inputs = self.data
        elif split == 'train':
            inputs = self.data.query(
                f"{self.data.index.names[0]} in "
                f"{list(self.train_labels.index.values)}")
        elif split == 'test':
            inputs = self.data.query(
                f"{self.data.index.names[0]} in "
                f"{list(self.test_labels.index.values)}")
        elif split == 'val':
            inputs = self.data.query(
                f"{self.data.index.names[0]} in "
                f"{list(self.val_labels.index.values)}")

        return inputs

    def get_labels_split(
            self, split: Literal['train', 'test', 'val', 'all'] = None):
        """Get original labels for a given data split.

        :param split: Selected split, defaults to None, as all data.
        :return: Selected labels.
        """
        logging.info('Getting labels from split...')
        if split is None or split == 'all':
            labels = self.labels
        elif split == 'train':
            labels = self.train_labels
        elif split == 'test':
            labels = self.test_labels
        elif split == 'val':
            labels = self.val_labels

        return labels

    def _read_csv(self, file, header, index_col, dtype):
        return pd.read_csv(file, header=header,
                           index_col=index_col, dtype=dtype)

    def load_data(self):
        # First columns in csv mus be index-columns in order,
        # with 2 or 4 levels Index: [sample_id, step_id, <h_id, w_id>]
        # Mandatory indexes: sample_id, step_id
        # The rest of columns can be 'class' and/or 'group' and features.
        # class and group will be in labels, separated from data.
        # If not existing, they will be autofilled.
        # Class column only allows `normal` or `abnormal` as possible values!!
        dtypes = {0: str, 'group': 'category', 'class': 'category',
                  'wwid': str, "[Tests]WWID": str}
        index_col = [0, 1]
        if self.load_2d:
            # Extra mandatory index for 2d data: h_id, w_id
            index_col.extend([2, 3])
            dtypes.update({i: np.uint8 for i in index_col[2:]})

        if not isinstance(self.data_path, list):
            self.data_path = [self.data_path]

        dfs = []
        dtypes = defaultdict(np.float32, dtypes)
        for data_path in self.data_path:
            data_path = Path(data_path)
            if data_path.is_file():
                df = self._read_csv(data_path, header=0, index_col=index_col,
                                    dtype=dtypes)
                dfs.append(df)
            else:
                all_files = sorted(data_path.glob("*.csv"))
                with Pool(processes=16) as pool:
                    dfs = pool.map(
                        partial(self._read_csv, header=0,
                                index_col=index_col, dtype=dtypes),
                        all_files)

        df = pd.concat(dfs)
        if sum(df.index.duplicated()) > 1:
            logging.warning('There are duplicated IDs in the provided files. '
                            'Appending a suffix per file to solve them. '
                            'Please check if this fits your needs!')
            for i in range(len(dfs)):
                all_indices = dfs[i].index.to_frame().to_numpy()
                all_indices[:, 0] = all_indices[:, 0] + '_' + str(i)
                dfs[i].index = pd.MultiIndex.from_arrays(
                    all_indices.T, names=dfs[i].index.names)

            df = pd.concat(dfs)

        if 'group' not in df:
            df['group'] = pd.Series(
                'Generic', index=df.index, dtype='category')
        if 'class' not in df:
            df['class'] = pd.Series('normal', index=df.index, dtype='category')

        self._data = df.drop(['class', 'group'], axis=1)
        self._labels = df[['class', 'group']
                          ].reset_index(level=index_col[1:], drop=True)
        self._labels = self._labels[
            ~self._labels.index.duplicated()]
        del df

    @staticmethod
    def get_dist(
            df: pd.DataFrame, label_col: str = None, normalize: bool = False):
        """Get distribution of dataframe.

        :param df: Dataframe to get the distribution from.
        :param label_col: Label field to use, defaults to None, as all fields.
        :param normalize: Normalize the distributions, defaults to False.
        :return: The Dataframe distribution.
        """
        samples = df.groupby(level=0).first()
        if label_col is None:
            label_col = samples.columns
        return samples[label_col].value_counts(normalize=normalize)

    def get_labels_dist(self,
                        split: Literal['train', 'test', 'val'] = None,
                        label_col: str = None,
                        normalize: bool = False):
        """Get labels distribution, for all data or selected split.

        :param split: Selected split, defaults to None, as all data.
        :param label_col: Label field to use, defaults to None, as all fields.
        :param normalize: Normalize the distributions, defaults to False.
        :raises ValueError: If a split distribution is requested and the data
            is not split.
        :return: The labels distribution.
        """
        if split is not None and not self.is_split:
            raise ValueError('The selected split is not created. '
                             'Split the data beforehand.')
        if split is None:
            labels_df = self.labels
        elif split == 'train':
            labels_df = self.train_labels
        elif split == 'test':
            labels_df = self.test_labels
        elif split == 'val':
            labels_df = self.val_labels

        return self.get_dist(
            labels_df, label_col=label_col, normalize=normalize)

    def get_labels_target_distrib(
            self, target_distrib: tuple = ('class', {'normal': 1})):
        """Get desired target labels distribution, filtering on a given column,
        typically class or group, with the specified values proportions.
        Note that class column only allows `normal` or `abnormal`.

        The specified values proportions sum must not exceed 1.
        It is possible to specify a proportion of 0 for a given value to
        exclude it from the target distribution.

        When the specified values proportions sum is lower than 1,
        samples will be added from the not specified groups, following their
        original stratification.

        The final target distribution total samples will be determined by the
        max number of samples that satisfies all proportions requirements.

        :param target_distrib: Tuple with the labels column to filter on and a
            dict of desired values proportion.
        :raises ValueError: If sum of desired values proportions is greater
            than 1.
        :return: Filtered labels that follows the requested target labels.
        """
        logging.info(f"Requesting a target distribution of {target_distrib}.")
        label_col, target_dist = target_distrib
        groups = sorted(target_dist.items(), key=lambda x: x[1], reverse=True)
        if np.sum([group[1] for group in groups]) > 1:
            raise ValueError(
                "Requested target distribution sums up more than 1.0")
        labels_dist = self.get_dist(self._labels, label_col=label_col)
        total_original_samples = labels_dist.sum()
        original_group_names = [group for group in labels_dist.index]

        total_samples = [total_original_samples]
        valid_groups = []
        seen_group_names = []
        for group, proportion in groups:
            seen_group_names.append(group)

            if group not in labels_dist:
                logging.warning(f"Group {group} of target distribution is "
                                "not part of labels.")
            elif proportion == 0:
                logging.warning(f"Requesting 0 samples from Group {group} "
                                "of target distribution.")
            else:
                valid_groups.append((group, proportion))
                total_samples.append(
                    (labels_dist[group] / proportion).astype(int))

        # Target total samples will be less or equal to total original samples.
        target_total_samples = min(total_samples)
        logging.info(f"Trying to get requested target distribution with "
                     f"maximum {target_total_samples} samples.")

        # If there are valid groups, sample them.
        target_ids = []
        for group, proportion in valid_groups:
            # How many samples we need of the current group.
            n_samples = np.ceil(proportion * target_total_samples).astype(int)
            # Samples ids of labels of the current group.
            labels = self._labels[self._labels[label_col] == group]
            # If there are samples of this group, sample randomly from them.
            if not labels.empty:
                ids = labels.index.to_frame(index=True)
                target_ids.append(ids.sample(
                    n=min(len(ids), n_samples), random_state=0))
            else:
                logging.warning(
                    f'Requested {group} is not part of {label_col}')

        if target_ids:
            target_ids = pd.concat(target_ids)
        else:
            target_ids = pd.DataFrame()

        # If still need more samples, sample them from rest of groups.
        not_seen_groups = [group for group in original_group_names
                           if group not in seen_group_names]

        if len(not_seen_groups) > 0 and len(target_ids) < target_total_samples:
            # Samples ids of labels of not seen groups.
            labels = self._labels[self._labels[label_col].isin(
                not_seen_groups)]
            ids = labels.index.to_frame(index=True)
            # How many samples we need of not seen groups.
            n_samples = min(target_total_samples - len(target_ids),
                            len(ids))
            ids = ids.sample(n=min(len(ids), n_samples), random_state=0)
            target_ids = pd.concat([target_ids, ids])

        logging.info("Final target distribution has "
                     f"{len(target_ids)} samples.")

        return target_ids.index

    def split_data(self, target_labels_index: pd.MultiIndex = None,
                   test_size: float = 0.2, val_random_state: int = 123):
        """Split data into Train, Test and Validation splits.
        First train and test splits with stratification are created, using
        test size, and then train split is divided again into train and
        validation, using same test size for validation.
        The random seed for this last split can be set.

        :param target_distrib: Tuple with the labels column to filter on and a
            dict of desired values proportion.
        :param test_size: Split size for test set. Defaults to 0.2.
        :param val_random_state: Random seed for train/validation split.
            Defaults to 123.
        """
        labels = self._labels
        unused_labels = None
        if target_labels_index is not None:
            labels = self._labels.loc[target_labels_index]
            unused_labels = self._labels.loc[
                self._labels.index.difference(labels.index)]

        # Shuffling before split, as the train_test_split will not shuffle
        # properly due to mask selection, keeping same input sorting.
        logging.info('Shuffling data...')
        labels = labels.sample(frac=1, random_state=0)

        logging.info('Splitting data...')
        # Stratified: same class and group distribution in splits as original.
        train_labels, self.test_labels = train_test_split(
            labels,
            stratify=labels,
            test_size=test_size,
            random_state=0)

        self.train_labels, self.val_labels = train_test_split(
            train_labels,
            stratify=train_labels,
            test_size=test_size,
            random_state=val_random_state)

        if unused_labels is not None:
            self.test_labels = pd.concat([self.test_labels, unused_labels])

        self.is_split = True

    def fit_scaler(self, columns: list[str], standard_scaler: bool = False,
                   fit_on_train: bool = True,
                   fit_scaler_on: dict = {'class': ['normal']}):
        """Fit Dataset scaler.

        :param columns: List of columns to use. Defaults to None,
            using all data columns.
        :param standard_scaler: Whether to use Standard scaler, or
            MinMaxScaler if False. Defaults to False (MinMaxScaler)
        :param fit_on_train: Whether to fit scaler only on train data.
            Defaults to True.
        :param fit_scaler_on: Dict with class and/or group values to filter
            the data for fitting the scaler. Defaults to {'class': ['normal']},
            so scaler will be fitted only on data from class normal.
            Note that class column only allows `normal` or `abnormal`.
        """
        logging.info('Fitting scaler...')
        if fit_on_train and not self.is_split:
            raise ValueError(
                'You have requested to fit the scaler on train data, '
                'but the data has not been split yet.')

        if columns is not None:
            self.columns = columns

        df = self.data[self.columns]

        scaler = StandardScaler() if standard_scaler else MinMaxScaler()
        if fit_on_train:
            scaler_df = self.train_labels.copy()
        else:
            scaler_df = self.labels.copy()
        # Currently applies sequentially, so order on the dict is important!
        if fit_scaler_on is not None:
            for column, values in fit_scaler_on.items():
                scaler_df = scaler_df.loc[scaler_df[column].isin(values)]

        scaler.fit(
            df.query(f"{df.index.names[0]} in {list(scaler_df.index.values)}"))

        self.scaler = scaler

    def scale_data(self, scaler: Union[MinMaxScaler, StandardScaler] = None):
        """Scale the data, either with a externally provided scaler,
        or with the dataset fitted scaler.

        :param scaler: External scaler to use, defaults to None
        :raises ValueError: If dataset has no fitted scaler and one is not
            provided as argument.
        """
        logging.info('Scaling data...')
        if scaler is None:
            scaler = self.scaler

        if scaler is None:
            raise ValueError('The scaler is not set. Provide a fitted scaler '
                             'or call fit_scaler.')

        df = self.data[self.columns]
        self.scaled_data = pd.DataFrame(scaler.transform(df),
                                        index=df.index,
                                        columns=df.columns)

    def prepare_data(self,
                     target_distrib: tuple = None,
                     split: bool = True,
                     test_size: float = 0.2,
                     val_random_state: int = 123,
                     columns: list[str] = None,
                     standard_scaler: bool = False,
                     fit_on_train: bool = True,
                     fit_scaler_on: dict = {'class': ['normal']}):
        """Prepare data, including splitting data, fitting scaler and scaling
        data.

        :param target_distrib: Tuple with the labels column to filter on and a
            dict of desired values proportion.
        :param split: Whether to split data into train, val and test.
            Defaults to True.
        :param test_size: Split size for test set. Defaults to 0.2.
        :param val_random_state: Random seed for train/validation split.
            Defaults to 123.
        :param columns: List of columns to use. Defaults to None,
            using all data columns.
        :param standard_scaler: Whether to use Standard scaler, or
            MinMaxScaler if False. Defaults to False (MinMaxScaler)
        :param fit_on_train: Whether to fit scaler only on train data.
            Defaults to True.
        :param fit_scaler_on: Dict with class and/or group values to filter
            the data for fitting the scaler. Defaults to {'class': ['normal']},
            so scaler will be fitted only on data from class normal.
            Note that class column only allows `normal` or `abnormal`.
        """
        logging.info('Preparing data...')

        # If requested target distribution set the target labels index.
        target_labels_index = None
        if target_distrib is not None:
            target_labels_index = self.get_labels_target_distrib(
                target_distrib=target_distrib)

        if split:
            # If requested target distribution, they are used for splitting.
            # but still all data will be used:
            # non-target data will be in the test split.
            self.split_data(target_labels_index=target_labels_index,
                            test_size=test_size,
                            val_random_state=val_random_state)
        elif target_labels_index is not None:
            # If split not done, and requested target distribution,
            # we want labels and data to be just filtered,
            # discarding the non-target data.
            self._data = self._data.query(
                f"{self._data.index.names[0]} in "
                f"{list(target_labels_index.values)}")
            self._labels = self._labels.loc[target_labels_index]

        if self.scaler is None:
            self.fit_scaler(columns=columns,
                            standard_scaler=standard_scaler,
                            fit_on_train=fit_on_train,
                            fit_scaler_on=fit_scaler_on)

        self.scale_data()

        # Update dataset config.
        self.target_distrib = target_distrib
        self.split = split
        self.test_size = test_size
        self.val_random_state = val_random_state
        self.standard_scaler = standard_scaler
        self.fit_on_train = fit_on_train
        self.fit_scaler_on = fit_scaler_on

        logging.info('Data is prepared!')

    def get_config(self):
        return {
            "dataset": {
                "name": self.name,
                "data_path": self.data_path,
                "load_2d": self.load_2d
            },
            "prepare_data": {
                "target_distrib": self.target_distrib,
                "split": self.split,
                "test_size": self.test_size,
                "val_random_state": self.val_random_state,
                "columns": self.columns,
                "standard_scaler": self.standard_scaler,
                "fit_on_train": self.fit_on_train,
                "fit_scaler_on": self.fit_scaler_on
            }
        }

    def save_scaler(self, path, overwrite=False):
        """Save scaler object as pickle.

        :param path: Path to folder where to save scaler.
        :param overwrite: Whether to overwrite file if exists,
            defaults to False.
        :raises ValueError: If scaler file already exists in path and
            overwrite is False.
        """
        path = Path(path)
        path.mkdir(exist_ok=True)
        scaler_path = path / 'scaler.pkl'
        if not overwrite and scaler_path.is_file():
            raise ValueError(
                f"Scaler already exists in {scaler_path}!")

        with scaler_path.open('wb') as scaler_f:
            pickle.dump(self.scaler, scaler_f)

    def load_scaler(self, path):
        """Load scaler file from path.

        :param path: Path to scaler file, or folder where it is located with
            the default name scaler.pkl.
        :raises ValueError: If file is not found.
        :return: Loaded scaler object.
        """
        path = Path(path)
        if path.is_dir():
            path = path / 'scaler.pkl'
        if not path.is_file():
            raise ValueError(
                f"{path} scaler file does not exist!")

        with path.open('rb') as scaler_f:
            return pickle.load(scaler_f)

    def save_config(self, overwrite=False):
        """Save dataset config file, in the default datasets folder.

        :raises ValueError: If the file already exists in the folder.
        """
        dataset_config_path = (DATASETS_PATH / self.name).with_suffix('.json')
        if dataset_config_path.is_file() and not overwrite:
            raise ValueError(f"Dataset {dataset_config_path.name} already "
                             f"exists in {DATASETS_PATH}!")

        with dataset_config_path.open('w') as config_f:
            json.dump(self.get_config(), config_f, indent=4)


def get_known_datasets():
    """Get a list of datasets config files available in the default datasets
    folder.

    :return: List of file names.
    """
    return sorted([dataset_file.stem for dataset_file
                   in (DATASETS_PATH).glob('*.json')])


def load_dataset(dataset_name: str, **kwargs) -> Dataset:
    """Get dataset.

    :param dataset_name: Name of dataset
    :param kwargs: Keyword args passed to Dataset constructor.
    :raises ValueError: if dataset_name is not found.
    :return: tuple (Dataset instance, Loaded dataset config)
    """
    logging.info('Loading dataset...')
    dataset_config = (DATASETS_PATH / dataset_name).with_suffix('.json')

    if not dataset_config.is_file():
        raise ValueError(
            f"Dataset {dataset_name} not existing in {DATASETS_PATH}!")

    with dataset_config.open('r') as config_f:
        config = json.load(config_f)

    return Dataset(**config['dataset'], **kwargs), config
