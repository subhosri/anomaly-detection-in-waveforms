import logging
from pathlib import Path
from typing import Literal

import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve

import common_models.autoencoder as ae
import core.utils as utils


# TODO: Fix and refactor:
def create_inputs(seq_index, model, bin_edges_0, bin_edges_1):
    print('---> create_inputs')

    samples = []
    values = np.random.rand(seq_index.shape[-1])
    df = pd.DataFrame(values, columns=['value'], index=seq_index)
    df = pd.concat([df], keys=['Random sample'], names=['path'])
    samples.append(df)

    values = np.random.uniform(0, 1, seq_index.shape[-1])
    df = pd.DataFrame(values, columns=['value'], index=seq_index)
    df = pd.concat([df], keys=['Random uniform'], names=['path'])
    samples.append(df)

    values = np.linspace(0, 1, seq_index.shape[-1])
    df = pd.DataFrame(values, columns=['value'], index=seq_index)
    df = pd.concat([df], keys=['Linspace'], names=['path'])
    samples.append(df)

    center_0 = (bin_edges_0[0] + bin_edges_0[-1]) / 2
    center_1 = (bin_edges_1[0] + bin_edges_1[-1]) / 2

    sampling_points = [[center_0, bin_edges_1[0] - 5], [center_0, center_1],
                       [center_0, bin_edges_1[-1] + 5],
                       [bin_edges_0[0] - 5, center_1],
                       [bin_edges_0[-1] + 5, center_1]]
    for sampling_point in sampling_points:
        values = ae.predict_from_latent(model, np.array([sampling_point])).T
        df = pd.DataFrame(values, columns=['value'], index=seq_index)
        df = pd.concat([df],
                       keys=[str(np.around(sampling_point, 4))],
                       names=['path'])
        samples.append(df)
    df = pd.concat(samples)

    return df


def load_model(model_path: str):
    """Load keras from model folder.

    :param model_path: Path to model folder
    :return: Loaded model.
    """
    logging.info('Loading model...')
    return ae.load_model(model_path)


def load_model_metadata(model_path: str):
    """Load metadata associated to a model

    :param model_path: Path to model folder
    :return: dict with metadata and train history dict.
    """
    logging.info('Loading model metadata...')

    model_path = Path(model_path)
    model = load_model(model_path)
    metadata = utils.load_json(model_path / 'training_config.json')

    # Insert model metadata at the beginning!
    model_summary = []
    model.summary(expand_nested=True,
                  print_fn=lambda x: model_summary.append(x))
    metadata = {**metadata, 'model_summary': model_summary}

    history = None
    history_path = model_path / 'history.json'
    if history_path.is_file():
        history = utils.load_json(history_path)

    return metadata, history


def compute_results(model: ae.Autoencoder,
                    X: pd.DataFrame,
                    scaler,
                    anomaly_criteria_metric: Literal['MSE'] = 'MSE'):
    """Get the model reconstructions for data and anomaly scores.

    :param model: Keras model to use.
    :param X: (Scaled) input data to model.
    :param scaler: Scaler to inverse transform the reconstructions.
    :param anomaly_criteria_metric: Anomaly criteria metric to use,
        defaults to 'MSE'
    :raises ValueError: If Anomaly criteria is unknown.
    :return: Reconstructions dataframe with latent space, and anomaly scores.
    """
    logging.info('Computing results...')
    used_features = ae.get_meta(model, 'columns')
    reconstructions, latent = _predict_with_model(model, X)

    # TODO: Offer other criteria: KL Loss, or selected rec loss used in model.
    # Based on (scaled) reconstruction, deviation to the input as MSE.
    if anomaly_criteria_metric == 'MSE':
        anomaly_criteria = np.square(X - reconstructions)
    else:
        raise ValueError('Unknown anomaly criteria!')

    # TODO: Reconstructed error on UNSCALED DATA!!
    # Recontructed error, on scaled data.
    reconstructions = reconstructions.join(X - reconstructions,
                                           lsuffix='_rec',
                                           rsuffix='_rec_error')

    # Scale reconstruction data back to the signal space.
    reconstructions = utils.inverse_transform(scaler,
                                              reconstructions,
                                              used_features,
                                              feat_suffixes=['rec'])

    # Join the latent.
    reconstructions = reconstructions.join(latent)

    return reconstructions, anomaly_criteria


def compute_anomaly_scores(
    reconstructions,
    anomaly_criteria,
    anomaly_levels:
        list[Literal['sample', 'sample_step', 'sample_pixel']] = ['sample']):
    """Compute the anomaly score(s), from the anomaly criteria values
    for the given levels. Finally join them to the data.

    :param reconstructions: Reconstruction dataframe.
    :param anomaly_criteria: Anomaly criteria dataframe.
    :param anomaly_levels: Anomaly levels to compute, defaults to 'sample'
    :return: Reconstructions dataframe with anomaly scores.
    """
    # TODO: Add scores per feature joins back if desired.
    logging.info('Computing anomaly scores...')
    # Sample anomaly score, per feat and aggregated, with sample_id index.
    # Sample anomaly score removes step and any other level in the index,
    # keeping a single value per sample, per feature and aggregated.
    if 'sample' in anomaly_levels:
        sample_score = anomaly_criteria.groupby(level=0, sort=False).mean()

        # If more than one column, keep per feat, and also aggregate.
        if len(anomaly_criteria.columns) > 1:
            sample_score = sample_score.add_suffix(
                '_sample_anomaly_score')
            # TODO: Add scores per feature joins back if desired.
            # reconstructions = reconstructions.join(sample_score)
            # Mean of columns
            sample_score = sample_score.mean(axis=1).to_frame()

        sample_score = sample_score.rename(
            columns={sample_score.columns[0]: 'sample_anomaly_score'})
        reconstructions = reconstructions.join(sample_score)

    # If more than one sequence step, aggregated anomaly score per step.
    # Sample step anomaly score sequence keeps sample and step axes in index,
    # removing any other level in the index, with a single value per step,
    # per feature and aggregated. Also the max value of the sequence is taken
    # as the sample step anomaly score for a given sample.
    if 'sample_step' in anomaly_levels and \
            len(anomaly_criteria.index.unique(level=1)) > 1:
        # If sequence data (2 levels in the index), already sample and step.
        sample_step_score = anomaly_criteria
        # If image-like data (4 levels index), groupby per sample and step.
        if anomaly_criteria.index.nlevels > 2:
            sample_step_score = anomaly_criteria.groupby(
                level=[0, 1], sort=False).mean()
        # If more than one column, keep per feat, and also aggregate.
        if len(anomaly_criteria.columns) > 1:
            sample_step_score = sample_step_score.add_suffix(
                '_sample_step_anomaly_score_seq')
            # TODO: Add scores per feature joins back if desired.
            # reconstructions = reconstructions.join(sample_step_score)
            # Mean of columns.
            sample_step_score = sample_step_score.mean(axis=1).to_frame()

        sample_step_score = sample_step_score.rename(
            columns={sample_step_score.columns[0]:
                     'sample_step_anomaly_score_seq'})
        reconstructions = reconstructions.join(sample_step_score)

        # Max to be used as sample criteria.
        max_step_score = sample_step_score.groupby(level=0).max()
        max_step_score = max_step_score.rename(
            columns={max_step_score.columns[0]: 'sample_step_anomaly_score'})
        reconstructions = reconstructions.join(max_step_score)

    # If image-like data (4 levels in the index), groupby per sample and pixel.
    # Sample pixel anomaly score sequence keeps sample and pixel (H and W)
    # axes in index, removing step level in the index, with a single value
    # per pixel position, per feature and aggregated.
    # Also the max value of the pixel sequence is taken as the
    # sample pixel anomaly score for a given sample.
    if 'sample_pixel' in anomaly_levels and anomaly_criteria.index.nlevels > 2:
        sample_pixel_score = anomaly_criteria.groupby(
            level=[0] + list(range(2, anomaly_criteria.index.nlevels)),
            sort=False).mean()

        # If more than one column, keep per feat, and also aggregate.
        if len(anomaly_criteria.columns) > 1:
            sample_pixel_score = \
                sample_pixel_score.add_suffix(
                    '_sample_pixel_anomaly_score_pxseq')
            # TODO: Add scores per feature joins back if desired.
            # reconstructions = reconstructions.join(
            #     sample_pixel_score, on=sample_pixel_score.index.names)
            # Mean of columns.
            sample_pixel_score = sample_pixel_score.mean(
                axis=1).to_frame()

        sample_pixel_score = sample_pixel_score.rename(
            columns={sample_pixel_score.columns[0]:
                     'sample_pixel_anomaly_score_pxseq'})
        reconstructions = reconstructions.join(
            sample_pixel_score, on=sample_pixel_score.index.names)

        # Max to be used as sample criteria.
        max_pixel_score = sample_pixel_score.groupby(level=0).max()
        max_pixel_score = max_pixel_score.rename(
            columns={max_pixel_score.columns[0]: "sample_pixel_anomaly_score"})
        reconstructions = reconstructions.join(max_pixel_score)

    return reconstructions


def _predict_with_model(model, X):
    X_np = utils.convert_to_numpy(X)

    # Predict the latent space and the output
    h_pred = model.m_enc.predict(X_np, batch_size=1024)
    x_pred = ae.predict_from_latent(model, h_pred)

    latent = pd.DataFrame(
        {f'latent_{idx}': np.repeat(h_pred[:, idx], np.prod(X_np.shape[1:-1]))
         for idx in range(h_pred.shape[1])}, index=X.index)

    reconstructions = pd.DataFrame(x_pred.reshape(X.shape),
                                   index=X.index,
                                   columns=X.columns)

    return reconstructions, latent


def get_latent_limits(data):
    """Get default grid configuration from (2D) latent space boundaries.

    :param data: Dataframe with latent points (latent_0 and latent_1).
    :return: list of tuples with the latent limits, for each axis:
            [(min_0, max_0), (min_1, max_1)
    """
    logging.info('Getting latent boundaries...')
    latent_0 = data['latent_0']
    latent_1 = data['latent_1']
    return [(float(latent_0.min()), float(latent_0.max())),
            (float(latent_1.min()), float(latent_1.max()))]


def create_latent_grid_edges(grid_config):
    """Create the latent grid edges from a (2D) latent grid config,
    as the grid subspaces boundaries

    :param grid_config: [(min_0, max_0, steps_0), (min_1, max_1, steps_1)
    :return: (bin_edges_0, bin_edges_1), tuple of arrays with the equispaced
        bin edges for both axes.
    """
    logging.info('Computing grid...')
    # Adding a small factor to include the max values in the last bin,
    # due to numpy digitize behaviour.
    bin_edges_0 = np.linspace(grid_config[0][0], grid_config[0][1] * 1.0025,
                              grid_config[0][2] + 1)

    bin_edges_1 = np.linspace(grid_config[1][0], grid_config[1][1] * 1.0025,
                              grid_config[1][2] + 1)

    return bin_edges_0, bin_edges_1


def count_per_bin(latent_bins):
    """Count samples per bin for the assigned latent bins in the grid.

    :param latent_bins: Dataframe with the assigned latent bin per sample,
        latent_bin_0 latent_bin_1 (optionally with more index levels).
    :return: The counts joined to input dataframe.
    """
    # Add counts per bin:
    counts = latent_bins.groupby(level=0, sort=False).first().groupby([
        'latent_bin_0', 'latent_bin_1']
    ).size().rename('samples_per_bin')
    return latent_bins.join(counts, on=['latent_bin_0', 'latent_bin_1'])


def assign_latent_bins_from_edges(latent, bin_edges_0, bin_edges_1):
    """Assign the samples latent points to a bin in the grid.

    :param latent: Dataframe with the latent space points per sample,
        latent_0 and latent_1 (optionally with more index levels).
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :return: The assigned latent grid bins, and count of samples per bin.
    """
    logging.info('Assigning bin in latent grid...')

    latent_bin_0 = np.digitize(
        latent['latent_0'], bins=bin_edges_0, right=False) - 1
    latent_bin_1 = np.digitize(
        latent['latent_1'], bins=bin_edges_1, right=False) - 1
    latent_bins = pd.DataFrame(
        {'latent_bin_0': latent_bin_0, 'latent_bin_1': latent_bin_1},
        index=latent.index)
    latent_bins = count_per_bin(latent_bins)

    latent = latent.join(latent_bins)

    return latent


def compute_means(model, data, scaler):
    """Compute means of data on each bin of the latent grid, and
    the generated reconstructions of the latent points means.

    :param model: Keras model to use for reconstructions of latent means.
    :param data: Dataframe with assigned latent bins (latent_bin_0 and
        latent_bin_1).
    :param scaler: Scaler to inverse transform the reconstructions.
    :return: Means dataframe, with latent_bin_0, latent_bin_1 as main indexes
        levels, with other secondary indexes, except the sample index.
    """
    logging.info('Computing means...')
    # 1) Mean of inputs and reconstructions
    means = data.groupby(
        ['latent_bin_0', 'latent_bin_1'] +
        data.index.names[1:]
    ).mean(numeric_only=True).add_suffix('_mean')

    # 2) Generated from mean of latent coordinates of inputs (encoded inputs).
    latent_mean = means.groupby(
        ['latent_bin_0', 'latent_bin_1']
    ).first()[['latent_0_mean', 'latent_1_mean']]

    generated_mean = ae.predict_from_latent(model, latent_mean.values)

    used_features = ae.get_meta(model, 'columns')
    generated_mean = pd.DataFrame(
        generated_mean.reshape(-1, len(used_features)),
        index=means.index,
        columns=used_features).add_suffix('_gen')

    generated_mean = utils.inverse_transform(scaler,
                                             generated_mean,
                                             used_features,
                                             feat_suffixes=['gen'])
    means = means.join(generated_mean)

    return means


def generate_equispaced_samples_mean(model, index,
                                     bin_edges_0, bin_edges_1,
                                     sampling_granularity, scaler):
    """Generate equispaced latent points sampled on each latent grid,
    compute their reconstructions and the mean of this reconstructions.

    :param model: Keras model to use for reconstructions of latent points.
    :param index: pd.Index with the expected indexes for a sample.
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :param sampling_granularity: Granularity of sampling per bin and per axis,
        in the latent space.
        The number of samples per bin will be then sampling_granularity ^ 2.
    :param scaler: Scaler to inverse transform the reconstructions.
    :return: Mean of equispaced latent samples resconstructions.
    """
    logging.info('Generating equispaced sampling mean...')
    # Mean of generated equispaced sampling in latent space.
    offset = (bin_edges_0[-1] - bin_edges_0[0]) / \
        (sampling_granularity * (len(bin_edges_0) - 1)) / 2
    sampling_points_0 = np.linspace(
        bin_edges_0[0], bin_edges_0[-1],
        sampling_granularity * (len(bin_edges_0) - 1), endpoint=False)
    sampling_points_0 += offset

    offset = (bin_edges_1[-1] - bin_edges_1[0]) / \
        (sampling_granularity * (len(bin_edges_1) - 1)) / 2
    sampling_points_1 = np.linspace(
        bin_edges_1[0], bin_edges_1[-1],
        sampling_granularity * (len(bin_edges_1) - 1), endpoint=False)
    sampling_points_1 += offset

    xv, yv = np.meshgrid(sampling_points_0, sampling_points_1, sparse=False)
    sampling = pd.DataFrame(dict(latent_0=xv.ravel(), latent_1=yv.ravel()))
    sampling = assign_latent_bins_from_edges(
        sampling, bin_edges_0, bin_edges_1)

    # Generate the synthetic samples.
    equispaced_samples = ae.predict_from_latent(
        model, sampling[['latent_0', 'latent_1']].values)

    secondary_indexes = [index.unique(level=level)
                         for level in range(1, index.nlevels)]
    mi = pd.MultiIndex.from_product([sampling.index] + secondary_indexes,
                                    names=index.names)
    used_features = ae.get_meta(model, 'columns')
    equispaced_samples = pd.DataFrame(
        equispaced_samples.reshape(-1, len(used_features)),
        index=mi,
        columns=used_features)

    # Join to get the latent_bins.
    equispaced_samples = equispaced_samples.join(
        sampling[['latent_bin_0', 'latent_bin_1']], on=index.names[0])
    # Compute the mean sequence from the sythetic sequences.
    equispaced_samples_mean = equispaced_samples.groupby(
        ['latent_bin_0', 'latent_bin_1'] +
        index.droplevel(0).names
    ).mean().add_suffix('_equispaced')
    # Scale data back to the signal space
    equispaced_samples_mean = utils.inverse_transform(
        scaler, equispaced_samples_mean,
        used_features, feat_suffixes=['equispaced'])
    return equispaced_samples_mean


def get_available_anomaly_levels(ds):
    """Get available anomaly levels from the dataset expected shape,
    sample level, sample_step level, sample_pixel level.

    :param ds: Dataset instance.
    :return: List of levels.
    """
    logging.info('Getting available anomaly levels...')
    levels = ['sample']
    if len(ds.index.unique(level=1)) > 1:
        levels.append('sample_step')
    if ds.index.nlevels > 2:
        levels.append('sample_pixel')
    return levels


def get_default_threshold_config(ds, data, th_level, th_std_factor=3):
    """Get the default threshold config, from the selected anomaly level score,
    as the mean of anomaly score + the factored std deviation of the dataset
    computed results.

    :param ds: Dataset instance.
    :param data: Dataframe with the anomaly score to threshold.
    :param th_level: Chosen anomaly level (sample, sample_step, sample_pixel).
    :param th_std_factor: Factor for std deviation, defaults to 3
    :return: anomaly_score_metric: the name of the anomaly score metric used
        (chosen given the th_level)
            max_threshold: Maximum value of anomaly_score_metric.
            default_threshold: mean(anomaly_score_metric) +
                th_std_factor * std(anomaly_score_metric)
    """
    logging.info('Getting threshold config...')
    anomaly_score_metric = th_level + '_anomaly_score'
    if ds.is_split:
        # Note that class column only allows `normal` or `abnormal`.
        if 'class' in ds.val_labels.columns:
            selection = ds.val_labels[
                ds.val_labels['class'] == 'normal'].index
        data = data.query(
            f"{data.index.names[0]} in {list(selection.values)}")

    anomaly_score = data[anomaly_score_metric]

    max_threshold = anomaly_score.max()
    default_threshold = np.mean(anomaly_score) + \
        th_std_factor * np.std(anomaly_score)
    return anomaly_score_metric, float(max_threshold), float(default_threshold)


def detect_anomalies(samples, anomaly_score_metric, threshold):
    """Classify samples as anomalous or not, by filtering by thresholds

    :param samples: Dataframe of samples to classify.
    :param anomaly_score_metric: Anomaly score metric to threshold.
    :param threshold: Threshold value.
    :return: samples, with a new "anomalous" column with True/False.
    """
    logging.info('Detecting anomalies...')
    samples['anomalous'] = samples[anomaly_score_metric] > threshold
    return samples


def compute_anomalies_stats(samples, anomaly_score_metric):
    """Compute anomalies statistics

    :param samples: Dataframe of samples.
    :param anomaly_score_metric: Chosen anomaly score metric
        (sample, sample_step, sample_pixel).
    :return: dict with statistics.
    """
    logging.info('Computing anomalies statistics...')
    stats = {}
    stats['num_samples'] = len(samples)
    stats['num_anomalies'] = samples['anomalous'].sum()
    stats['pct_anomalies'] = samples['anomalous'].sum() / len(samples)
    if 'class' in samples.columns:
        # Note that class column only allows `normal` or `abnormal`.
        normal_data = samples[samples['class'] == 'normal']
        stats['FP'] = (normal_data['anomalous']).sum()
        stats['TN'] = (~normal_data['anomalous']).sum()
        stats['FPR'] = stats['FP'].sum() / (stats['FP'] + stats['TN'])
        stats['TNR'] = stats['TN'].sum() / (stats['FP'] + stats['TN'])

        if 'abnormal' in samples['class'].values:
            abnormal_data = samples[samples['class'] == 'abnormal']
            stats['FN'] = (~abnormal_data['anomalous']).sum()
            stats['TP'] = (abnormal_data['anomalous']).sum()
            stats['TPR'] = stats['TP'].sum() / (stats['TP'] + stats['FN'])
            stats['FNR'] = stats['FN'].sum() / (stats['TP'] + stats['FN'])

            stats['ROC'] = compute_roc(samples, anomaly_score_metric)

    stats[f'mean_{anomaly_score_metric}'] = np.mean(
        samples[anomaly_score_metric])
    stats[f'std_dev_{anomaly_score_metric}'] = np.std(
        samples[anomaly_score_metric])
    stats[f'max_{anomaly_score_metric}'] = samples[anomaly_score_metric].max()
    return stats


def compute_roc(samples, anomaly_score_metric):
    """Comput ROC, based on class label of samples and the desired anomaly
    score metric column.
    Note that class column only allows `normal` or `abnormal`.

    :param samples: Dataframe, with class and the desired anomaly score metric.
    :param anomaly_score_metric: Desired anomaly score metric column name.
    :return: List of 3 arrays, as computed [FPR, TPR, Thresholds]
    """
    logging.info('Computing ROC...')
    y_true = samples['class'] == 'abnormal'
    y_score = samples[anomaly_score_metric]
    fpr, tpr, thresholds = roc_curve(y_true, y_score, drop_intermediate=False)
    return [fpr, tpr, thresholds]
