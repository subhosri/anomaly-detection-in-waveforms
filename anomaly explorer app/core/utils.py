import json
from pathlib import Path

import pandas as pd
import sklearn.model_selection


def get_level_values(index, levels):
    """Returns the index values for the specified list of levels from a
    pandas.MultiIndex, while for pandas.Index it returns the same Index, as it
    would contain only one level.

    This is primarily useful to get levels of values from a MultiIndex,
    but is provided on Index as well for compatibility.

    This does not seem to exist in vanilla pandas for some reason.
    get_level_values only supports one level for MultiIndex.

    Taken from Retina.

    Args:
        index (pandas.Index|MultiIndex): Index to work on.
        levels (list): List of levels.

    Returns:
        pandas.Index|MultiIndex: A MultiIndex|Index containing the specified
            levels.
    """
    # If levels as ints, get the corresponding index names:
    for i, level in enumerate(levels):
        if isinstance(level, int):
            levels[i] = index.names[level]

    if not isinstance(index, pd.MultiIndex):
        level_values = index.get_level_values(levels[0])
    else:
        level_values = pd.MultiIndex.from_arrays(list(
            map(lambda x: index.get_level_values(x).values, levels)),
            names=levels)
    return level_values


def _filter_by_index_value(df, authorized_index):
    """
    Filters all rows where the index values at the the authorized_index levels
    are not in authorized_index values, with a binary mask.
    The dataframe index must contain at least the same level(s) as the
    authorized_index, but can have more.

    Taken from Retina.

    Args:
        df (pandas.DataFrame): DataFrame to be filtered.
        authorized_index (pd.Index|pd.MultiIndex): Index containing allowed
            values.
    Returns:
        pandas.DataFrame: The filtered dataframe
    """
    mask = get_level_values(df.index,
                            authorized_index.names).isin(authorized_index)
    return df.loc[mask]


def _index_in(df1, df2, levels):
    return get_level_values(df1.index,
                            levels).isin(get_level_values(df2.index, levels))


def train_test_split(X_df, levels, y_df=None, **kwargs):
    """Splits pandas.DataFrame with index into 2 subsets.

        Taken from Retina.

    Args:
        X_df (pandas.DataFrame): Input data Dataframe to split
        levels (list[str]): List of levels to perform the split on.
        y_df (pandas.DataFrame, optional): Defaults to None.
            Labels Dataframe to split.
        **kwargs: passed on to sklearn.model_selection.train_test_split

    Returns:
        tuple: tuple containing
            pandas.DataFrame: X_train.
            pandas.DataFrame: X_test.
            pandas.DataFrame: y_train, if y_df is provided, else None.
            pandas.DataFrame: y_test, if y_df is provided, else None.
    """
    if y_df is not None:
        if not all(_index_in(X_df, y_df, levels)) or not all(
                _index_in(y_df, X_df, levels)):
            raise ValueError("{} must contain the same elements in both "
                             "dataframes".format(levels))

    # assuming that X and y contain the same Ids.
    split_ids = get_level_values(X_df.index, levels).unique()

    train_ids, test_ids = sklearn.model_selection.train_test_split(
        split_ids, **kwargs)

    X_train = _filter_by_index_value(X_df, train_ids)
    X_test = _filter_by_index_value(X_df, test_ids)

    y_train = None
    y_test = None
    if y_df is not None:
        y_train = _filter_by_index_value(y_df, train_ids)
        y_test = _filter_by_index_value(y_df, test_ids)

    return X_train, X_test, y_train, y_test


def convert_to_numpy(X):
    """Convert Dataframe to numpy array, extracting the expected shape from
    the index structure.

    :param X: Dataframe to convert
    :return: Numpy ndarray.
    """
    expected_shape = [len(X.index.unique(level=level))
                      for level in range(X.index.nlevels)] + [len(X.columns)]
    X_np = X.values.reshape(expected_shape)
    # If 2D, squeeze the seq axis, for conv2D layers.
    if X.index.nlevels > 2 and expected_shape[1] == 1:
        X_np = X_np.squeeze(axis=1)

    return X_np


def inverse_transform(scaler,
                      samples,
                      used_features,
                      feat_suffixes=['', 'rec']):
    """Apply the inverse transform of the scaler to the samples
    dataframe corresponding columns denoted by used_features.
    """
    for feat_suffix in feat_suffixes:
        features = [
            f"{cur_feature}{'_' if feat_suffix else ''}{feat_suffix}"
            for cur_feature in used_features
        ]
        samples.loc[:,
                    features] = scaler.inverse_transform(samples.loc[:,
                                                                     features])

    return samples


def save_json(path, obj):
    """Save object as json file.

    :param path: Path to file.
    :param obj: Object to convert.
    """
    path.parent.mkdir(exist_ok=True)
    with path.open('w') as f:
        json.dump(obj, f, indent=4)


def load_json(path):
    """Load json file.

    :param path: Path to file to load.
    :raises ValueError: If file does not exist.
    :return: Loaded object.
    """
    path = Path(path)
    if not path.is_file():
        raise ValueError(
            f"{path} file does not exist!")

    with path.open('r') as f:
        return json.load(f)
