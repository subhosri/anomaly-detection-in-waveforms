import logging

import pandas as pd
import plotly.express as px
from plotly.subplots import make_subplots

import app.ui as ui

# import plotly
# plotly.offline.init_notebook_mode()


def plot_loss(history):
    """Plot history loss.

    :param history: Dict with history with training metrics as keys.
    :return: Plot fig.
    """
    logging.info('Plotting loss history...')
    # Check if there are any validation plots in the history
    has_val = any([
        metric.startswith('val_') for metric in history.keys()
    ])

    metrics = [
        metric for metric in history.keys() if not metric.startswith('val_')
    ]

    fig = make_subplots(rows=1, cols=len(metrics), subplot_titles=metrics)

    cols = 1
    for metric in metrics:
        y = [metric]
        if f'val_{metric}' in history.keys():
            y += [f'val_{metric}']
        fig.add_traces(px.line(
            history,
            y=y,
        ).data, rows=1, cols=cols)
        cols += 1

    fig.for_each_trace(lambda trace: trace.update(
        name='Validation',
        legendgroup='Validation',
    ) if 'val_' in trace.name else trace.update(
        name='Training',
        legendgroup='Training',
    ))
    fig.update_traces(
        showlegend=False,
        hovertemplate='Loss: %{y:.5f}' + '<br>Epoch: %{x}<br>',
    )

    fig.update_traces(selector=-1 if not has_val else -
                      2, showlegend=True, name='Training')
    if has_val:
        fig.update_traces(selector=-1, showlegend=True, name='Validation')

    fig.update_layout(
        title='Training Losses',
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    return fig


def plot_latent_space(latent,
                      anomaly_score_metric,
                      max_color_value,
                      anom_size=4,
                      divide_by_group=False):
    """Plot (2D) latent space, colored by the anomaly metric,
    with different symbols (and sizes) for normal and abnormal data (based on
    detection), and optionally grouped by the group field.


    :param latent: Dataframe with the latent information per sample.
    :param anomaly_score_metric: Anomaly score metric name to use as color.
    :param max_color_value: Max value to set in the coloraxis.
    :param anom_size: Factor to points size if anomalous, defaults to 4.
    :param divide_by_group: Create traces by group field, defaults to False.
    :return: Latent space figure.
    """
    logging.info('Plotting Latent Space...')
    instance_axis = latent.index.names[0]
    latent = latent.reset_index()

    # We distinguish between TN, FN, TP and FP - which however only makes
    # sense if we have known anomalies
    # Note that class column only allows `normal` or `abnormal`.
    has_known_anomalies = (latent['class'] == 'abnormal').any()

    # Compare with the real labels to derive true/false positives/negatives
    latent['Result'] = 'TN' if has_known_anomalies else 'Normal'
    latent.loc[(~latent['anomalous']) &
               (latent['class'] == 'abnormal'), 'Result'] = 'FN'
    latent.loc[(latent['anomalous']) &
               (latent['class'] == 'abnormal'), 'Result'] = 'TP'
    latent.loc[(latent['anomalous']) &
               (latent['class'] == 'normal'),
               'Result'] = 'FP' if has_known_anomalies else 'anomalous'
    # We could attach the known groups, but this may result in a lot of labels
    if divide_by_group:
        latent['Result'] = latent['Result'] + \
            ': ' + latent['group'].astype(str)
    # Detected anomalies should be highlighted in the plot by increased size
    latent['size'] = (latent['anomalous']).astype('float') * anom_size + 2

    # Show meta information in the hover box
    anomaly_scores = sorted(
        [column for column in latent.columns if 'anomaly_score' in column])
    custom_data = [
        'anomalous', 'latent_bin_0', 'latent_bin_1',
        'group', 'class', instance_axis
    ] + anomaly_scores

    hovertemplate = '<br>'.join([
        '<b>%{customdata[5]}</b>',
        'Anomalous: %{customdata[0]}',
        'Latent dim 0: %{x:.5f}', 'Latent dim 1: %{y:.5f}',
        'Latent col: %{customdata[1]}',
        'Latent row: %{customdata[2]}',
        'Group: %{customdata[3]}',
        'Class: %{customdata[4]}'])

    i = 6
    for anomaly_score in anomaly_scores:
        hovertemplate += '<br>' + \
            ui.split_and_capitalize(anomaly_score) + \
            f': %{{customdata[{i}]:.5f}}'
        i += 1

    # Color: reconstruction error; size: detected anomalies
    scat = px.scatter(latent,
                      x='latent_0',
                      y='latent_1',
                      size='size',
                      size_max=11,
                      color=anomaly_score_metric,
                      color_continuous_scale='rdylgn_r',
                      symbol='Result',
                      custom_data=custom_data,
                      hover_name=instance_axis,
                      opacity=0.5,
                      template='plotly_white')
    scat.update_traces(hovertemplate=hovertemplate)

    scat.update_layout(
        autosize=True,
        margin=dict(b=0, t=0, l=0, r=0),
        yaxis_scaleanchor='x',
        height=700,
        width=700,
        template='simple_white',
        legend=dict(orientation='h'),
        coloraxis_cmin=0,
        coloraxis_cmax=max_color_value,
        coloraxis_colorbar_title_text='',
        coloraxis_colorbar_x=0.95,
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )

    return scat


def add_grid_to_scatter(fig, bin_edges_0, bin_edges_1):
    """Add to fig the grid lines defined by the edges arrays,

    :param fig: Figure to update.
    :param bin_edges_0: arrays with the equispaced bin edges for axis 0.
    :param bin_edges_1: arrays with the equispaced bin edges for axis 1.
    :return: Updated figure.
    """
    logging.info('Adding grid to Latent Space...')
    for value in bin_edges_0:
        fig.add_vline(x=value, line_width=1)
    for value in bin_edges_1:
        fig.add_hline(y=value, line_width=1)
    return fig


def plot_latent_grid_means(means, column, fig=None):
    """Plot latent grid with means, and optionally anomalies and
    reconstructions, and heatmaps, for a given feature column.

    :param means: Means dataframe.
    :param column: Feature column name.
    :param fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    logging.info('Plotting Means in Latent Grid...')
    # Reset indexes.
    seq_axis = means.index.names[-1]
    means = means.reset_index()
    add_markers = means[seq_axis].nunique() < 2
    # Data preparation
    renamer = {
        column + '_mean': 'Mean of Inputs',
        column + '_rec_mean': 'Mean of Reconstructions',
        column + '_gen': 'Decoded mean of latent points',
        column + '_equispaced': 'Mean of decoded equispaced latent samples',
    }
    new_columns = list(renamer.values())
    means = means.sort_values(by=['latent_bin_0', 'latent_bin_1'],
                              ascending=[True, False]).rename(columns=renamer)
    category_orders = {
        'latent_bin_0':
        list(range(min(means['latent_bin_0']),
                   max(means['latent_bin_0']) + 1)),
        'latent_bin_1':
        list(
            reversed(
                range(min(means['latent_bin_1']),
                      max(means['latent_bin_1']) + 1))),
    }
    # Plotting means
    means_fig = px.line(
        means,
        x=seq_axis,
        y=new_columns,
        facet_col='latent_bin_0',
        facet_row='latent_bin_1',
        category_orders=category_orders,
        # hover_name='variable',
        labels={
            'variable': '',
            'latent_bin_0': 'Latent col',
            'latent_bin_1': 'Latent row',
        },
        hover_data={
            'variable': False,
            'latent_bin_0': True,
            'latent_bin_1': True,
        },
        markers=add_markers
    )

    means_fig.for_each_annotation(
        lambda a: a.update(text=a.text.split('=')[-1]))
    means_fig.update_traces(
        selector={'name': new_columns[0]},
        #   line={'dash': 'dot'},
        visible=True,
    )
    means_fig.update_traces(
        selector={'name': new_columns[1]},
        #   line={'dash': 'solid'},
        visible=True)
    means_fig.update_traces(
        selector={'name': new_columns[2]},
        #   line={'dash': 'dash'},
        visible='legendonly')
    means_fig.update_traces(
        selector={'name': new_columns[3]},
        #   line={'dash': 'dot'},
        visible='legendonly')

    means_fig.update_traces(hovertemplate=None)

    if fig is not None:
        fig.add_traces(means_fig.data)
    else:
        fig = means_fig

    # Final layout
    fig.update_layout(
        autosize=True,
        height=700,
        width=700,
        margin=dict(b=0, t=20, l=0, r=0),
        template='simple_white',
        legend=dict(orientation='h'),
        hovermode='x',
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    fig.update_yaxes(title='', showticklabels=True)  # range=[0, 1],
    fig.update_xaxes(title='')

    return fig


def plot_latent_grid_2d(means, columns, fig=None):
    """Plot latent grid with means, for given feature columns.

    :param means: Means dataframe.
    :param columns: List of feature columns names.
    :param samples: Samples dataframe, to create annotations of number of
        anomalies/samples per bin, defaults to None
    :param fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    logging.info('Plotting Latent Grid 2D...')
    # Reset indexes.
    expected_shape = [len(means.index.unique(level=level))
                      for level in range(2, means.index.nlevels)]
    means = means.reset_index()
    # Data preparation
    renamer = {
        'mean': 'Mean of Inputs',
        'rec_mean': 'Mean of Reconstructions',
        'gen': 'Decoded mean of latent points',
        'equispaced': 'Mean of decoded equispaced latent samples',
    }
    col_suffixes = list(renamer.keys())
    means = means.sort_values(by=['latent_bin_0', 'latent_bin_1'],
                              ascending=[True, False])
    category_orders = {
        'latent_bin_0':
        list(range(min(means['latent_bin_0']),
                   max(means['latent_bin_0']) + 1)),
        'latent_bin_1':
        list(range(min(means['latent_bin_1']),
                   max(means['latent_bin_1']) + 1)),
    }

    means_fig = make_subplots(
        rows=len(category_orders['latent_bin_1']),
        cols=len(category_orders['latent_bin_0']),
        column_titles=[str(n) for n in category_orders['latent_bin_0']],
        row_titles=[str(n) for n in category_orders['latent_bin_1']],
        start_cell="bottom-left",
        shared_xaxes='all',
        shared_yaxes='all')

    legend_shown = [False] * len(col_suffixes)
    for i, x in enumerate(category_orders['latent_bin_0']):
        for j, y in enumerate(category_orders['latent_bin_1']):
            m = means.loc[(means['latent_bin_0'] == x)
                          & (means['latent_bin_1'] == y)]
            for k, col_suffix in enumerate(col_suffixes):
                cols = [f'{col}_{col_suffix}' for col in columns]
                if m[cols].notnull().any().any():
                    m_plot = px.imshow(
                        m[cols].values.reshape(expected_shape),
                        aspect='equal', animation_frame=0)
                    m_plot.data[0]['name'] = f'{renamer[col_suffix]}'
                    means_fig.add_trace(m_plot.data[0], row=j + 1, col=i + 1)
                    means_fig.update_traces(selector=-1,
                                            legendgroup=renamer[col_suffix],
                                            visible='legendonly',
                                            showlegend=not legend_shown[k])
                    legend_shown[k] = f'{renamer[col_suffix]}'

    means_fig.update_traces(selector={'name': f"{renamer['mean']}"},
                            visible=True)

    if fig is not None:
        fig.add_traces(means_fig.data)
    else:
        fig = means_fig

    # Final layout
    fig.update_layout(
        autosize=True,
        height=700,
        width=700,
        margin=dict(b=0, t=20, l=0, r=0),
        template='simple_white',
        legend=dict(orientation='h'),
        hovermode='x',
        coloraxis_colorscale='viridis',
        coloraxis_cauto=False,
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    fig.update_xaxes(visible=False)
    fig.update_yaxes(visible=False)

    return fig


def add_samples_annotations(samples, fig):
    """Add samples counts annotations to latent grid.

    :param samples: Samples dataframe (with inputs and reconstructions).
    :param fig: Figure to update
    :return: Latent grid figure.
    """
    logging.info('Counting samples in Latent Grid...')
    # Adding counts annotations
    count_samples = samples.reset_index()[
        [samples.index.names[0], 'latent_bin_0', 'latent_bin_1', 'anomalous']]
    count_samples = count_samples.drop_duplicates()[
        ['latent_bin_0', 'latent_bin_1', 'anomalous']]
    count_samples = count_samples.value_counts()
    count_samples = count_samples.unstack(level=-1, fill_value=0)
    if True not in count_samples:
        count_samples[True] = 0
    if False not in count_samples:
        count_samples[False] = 0
    count_samples['Total'] = count_samples.sum(axis=1)
    count_samples = count_samples.reset_index()

    for _, count in count_samples.iterrows():
        if count['Total'] < 1:
            continue
        fig.add_annotation(
            x=0,
            xanchor='left',
            xref='x domain',
            y=1,
            yanchor='middle',
            yref='y domain',
            showarrow=False,
            col=count['latent_bin_0'] + 1 - min(count_samples['latent_bin_0']),
            row=count['latent_bin_1'] + 1 - min(count_samples['latent_bin_1']),
            text=f'<span style="color:red">{count[True]}</span>'
            f'/{count["Total"]}',
            font={
                'size': 16,
            })

    return fig


def _prepare_samples_for_latent_grid(samples, anomalies_only=True):
    instance_axis = samples.index.names[0]
    seq_axis = samples.index.names[-1]
    category_orders = {
        'latent_bin_0':
        list(
            range(min(samples['latent_bin_0']),
                  max(samples['latent_bin_0']) + 1)),
        'latent_bin_1':
        list(
            reversed(
                range(min(samples['latent_bin_1']),
                      max(samples['latent_bin_1']) + 1))),
    }

    # Show only anomalies.
    if anomalies_only:
        samples = samples[samples['anomalous']]

    samples = samples.reset_index()
    samples = samples.sort_values(
        by=['latent_bin_0', 'latent_bin_1'], ascending=[True, False])

    return samples, instance_axis, seq_axis, category_orders


def plot_latent_grid_samples(samples,
                             column,
                             anomalies_only=True,
                             legend_prefix='Anomalies',
                             fig=None):
    """Plot latent grid with samples and reconstructions, for a given
    feature column.

    :param samples: Samples dataframe (with inputs and reconstructions).
    :param column: Feature column name.
    :param anomalies_only: Whether to plot only anomalies.
    :param legend_prefix: Prefix to prepend in legend entry.
    :param fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    logging.info(f'Plotting {legend_prefix} Samples in Latent Grid...')
    samples, instance_axis, seq_axis, category_orders = \
        _prepare_samples_for_latent_grid(
            samples, anomalies_only=anomalies_only)

    add_markers = samples[seq_axis].nunique() < 2
    # Show meta information in the hover box
    anomaly_scores = sorted(
        [column for column in samples.columns if 'anomaly_score' in column])
    custom_data = [
        'anomalous', column, column + '_rec', 'group', 'class'
    ] + anomaly_scores

    hovertemplate = '<br>'.join([
        'Anomalous: %{customdata[0]}',
        'Input value: %{customdata[1]:.5f}',
        'Reconstructed value: %{customdata[2]:.5f}',
        'Group: %{customdata[3]}',
        'Class: %{customdata[4]}'])

    i = 5
    for anomaly_score in anomaly_scores:
        hovertemplate += '<br>' + \
            ui.split_and_capitalize(anomaly_score) + \
            f': %{{customdata[{i}]:.5f}}'
        i += 1

    # Plotting samples and their reconstructions
    samples_traces = px.line(samples,
                             x=seq_axis,
                             y=column,
                             line_group=instance_axis,
                             facet_col='latent_bin_0',
                             facet_row='latent_bin_1',
                             category_orders=category_orders,
                             color=instance_axis,
                             color_discrete_sequence=px.colors.qualitative.G10,
                             hover_name=instance_axis,
                             custom_data=custom_data,
                             markers=add_markers)

    samples_traces.update_traces(legendgroup=f'{legend_prefix} Inputs',
                                 visible='legendonly',
                                 showlegend=False,
                                 hovertemplate=hovertemplate)
    samples_traces.update_traces(selector=-1,
                                 showlegend=True,
                                 name=f'{legend_prefix} Inputs')

    if fig is not None:
        fig.add_traces(samples_traces.data)
    else:
        fig = samples_traces

    reconstructed_traces = px.line(
        samples,
        x=seq_axis,
        y=column + '_rec',
        line_group=instance_axis,
        facet_col='latent_bin_0',
        facet_row='latent_bin_1',
        category_orders=category_orders,
        color=instance_axis,
        color_discrete_sequence=px.colors.qualitative.G10,
        hover_name=instance_axis,
        custom_data=custom_data,
        markers=add_markers)

    reconstructed_traces.update_traces(
        legendgroup=f'{legend_prefix} Reconstructions',
        visible='legendonly',
        showlegend=False,
        line_dash='dot',
        hovertemplate='Reconstructed value: %{customdata[2]:.5f}')
    reconstructed_traces.update_traces(selector=-1,
                                       showlegend=True,
                                       name=f'{legend_prefix} Reconstructions')
    fig.add_traces(reconstructed_traces.data)

    return fig


def plot_latent_grid_heatmap(samples,
                             column,
                             anomalies_only=True,
                             legend_prefix='Anomalies',
                             fig=None):
    """Plot latent grid with samples heatmaps, for a given feature column.

    :param samples: Samples dataframe (with inputs and reconstructions),
    :param column: Feature column name.
    :param anomalies_only: Whether to plot only anomalies heatmaps.
    :param legend_prefix: Prefix to prepend in legend entry.
    :param fig: Figure to update, defaults to None.
    :return: Latent grid figure.
    """
    logging.info(f'Plotting {legend_prefix} Heatmaps in Latent Grid...')
    samples, instance_axis, seq_axis, category_orders = \
        _prepare_samples_for_latent_grid(
            samples, anomalies_only=anomalies_only)

    samples_heatmap = px.density_heatmap(
        samples,
        x=seq_axis,
        y=column,
        facet_col='latent_bin_0',
        facet_row='latent_bin_1',
        category_orders=category_orders,
        labels={seq_axis: ui.split_and_capitalize(seq_axis)},
        hover_data={
            'latent_bin_0': False,
            'latent_bin_1': False,
        },
    )

    samples_heatmap.update_traces(legendgroup=f'{legend_prefix} Heatmap',
                                  visible='legendonly',
                                  showlegend=False,
                                  showscale=False)
    samples_heatmap.update_traces(selector=-1,
                                  showlegend=True,
                                  name=f'{legend_prefix} Heatmap')
    samples_heatmap.for_each_trace(
        lambda trace: trace.update(ybingroup=trace.name))

    if fig is not None:
        fig.add_traces(samples_heatmap.data)
    else:
        fig = samples_heatmap

    color_scale = px.colors.sequential.Viridis.copy()
    # Option 1: Insert a new color, transparent.
    # color_scale.insert(0, 'rgba(0, 0, 0, 0)')
    # Option 2: Change existing initial color to transparent.
    # init_color = px.colors.hex_to_rgb(color_scale[0])
    # color_scale[
    #     0] = f'rgba({init_color[0]}, {init_color[1]}, {init_color[2]}, 0)'
    # Option 3: Substitute initial color by new transparent color.
    color_scale[0] = 'rgba(0, 0, 0, 0)'
    fig.update_layout(coloraxis_colorscale=color_scale)

    return fig


def plot_sample(samples, column):
    """Plot sample sequences for a given feature column.

    :param samples: Samples dataframe (typically with one sample).
    :param column: Feature column name.
    :return: Sample figure.
    """
    logging.info('Plotting sample...')
    # Reset indexes.
    seq_axis = samples.index.names[-1]
    instance_axis = samples.index.names[0]
    samples = samples.reset_index()
    renamer = {
        column + '_equispaced': 'Mean of decoded equispaced latent samples',
        column + '_mean': 'Mean of Inputs'
    }
    new_columns = list(renamer.values())

    samples = samples.rename(columns=renamer)
    anomaly_scores = sorted(
        [column for column in samples.columns if 'anomaly_score' in column])
    custom_data = [
        'anomalous', 'latent_bin_0', 'latent_bin_1', 'group', 'class'
    ] + anomaly_scores

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    # Plotting samples and their reconstructions
    samples_traces = px.line(
        samples,
        x=seq_axis,
        y=column,
        line_group=instance_axis,
        color=instance_axis,
        color_discrete_sequence=['blue'],
        custom_data=custom_data,
    )

    samples_traces.for_each_trace(
        lambda trace: trace.update(legendgrouptitle_text=trace.name))

    hovertemplate = '<br>'.join([
        # '<extra></extra>'
        'Anomalous: %{customdata[0]}',
        'Input value: %{y:.5f}',
        'Latent col: %{customdata[1]}',
        'Latent row: %{customdata[2]}',
        'Group: %{customdata[3]}',
        'Class: %{customdata[4]}'])
    i = 5
    for anomaly_score in anomaly_scores:
        hovertemplate += '<br>' + \
            ui.split_and_capitalize(anomaly_score) + \
            f': %{{customdata[{i}]:.5f}}'
        i += 1

    samples_traces.update_traces(
        selector=0, name='Input', hovertemplate=hovertemplate)

    fig.add_traces(samples_traces.data)

    reconstructed_traces = px.line(
        samples,
        x=seq_axis,
        y=column + '_rec',
        line_group=instance_axis,
        color=instance_axis,
        color_discrete_sequence=['red'],
    )

    reconstructed_traces.update_traces(
        selector=0,
        #   line={'dash': 'dot'},
        # visible='legendonly',
        name='Reconstructed',
        hovertemplate=None,
        # hoverinfo='skip'
    )

    fig.add_traces(reconstructed_traces.data)

    extra = px.line(
        samples,
        x=seq_axis,
        y=new_columns,
        line_group=instance_axis,
        # color=instance_axis,
        color_discrete_sequence=['purple', 'yellow'],
    )

    extra.update_traces(
        visible='legendonly',
        hovertemplate=None,
    )

    fig.add_traces(extra.data)

    if 'sample_step_anomaly_score_seq' in samples:
        anomaly_score_traces = px.line(
            samples,
            x=seq_axis,
            y='sample_step_anomaly_score_seq',
            line_group=instance_axis,
            color=instance_axis,
            color_discrete_sequence=['green'],
        )

        anomaly_score_traces.update_traces(
            selector=0,
            #   line={'dash': 'dot'},
            # visible='legendonly',
            name='Sample Step Anomaly Score Seq',
            hovertemplate=None,
            # hoverinfo='skip'
        )

        fig.add_traces(anomaly_score_traces.data, secondary_ys=[True])

    # Final layout
    fig.update_layout(
        autosize=True,
        height=700,
        width=700,
        margin=dict(b=0, t=20, l=0, r=0),
        template='simple_white',
        legend=dict(orientation='h', title='',
                    groupclick="toggleitem"),
        hovermode='x',
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    fig.update_yaxes(title='')
    fig.update_xaxes(title='')
    return fig


def plot_sample_2d(samples, columns):
    """Plot a 2D sample for given feature channels columns.

    :param samples: Samples dataframe (typically with one sample).
    :param column: Feature column name.
    :return: Sample figure.
    """
    logging.info('Plotting sample...')
    expected_shape = [len(samples.index.unique(level=level))
                      for level in range(samples.index.nlevels)]
    if len(columns) > 1:
        expected_shape += [len(columns)]
    samples = samples.reset_index()
    anomaly_scores = sorted(
        [column for column in samples.columns if 'anomaly_score' in column])
    custom_data = samples[[
        'anomalous', 'latent_bin_0', 'latent_bin_1', 'group', 'class'
    ] + anomaly_scores].values.reshape(expected_shape[1:] + [-1])

    hovertemplate = '<br>'.join([
        # '<extra></extra>'
        'Anomalous: %{customdata[0]}',
        'Input value: %{z:.5f}',
        'Latent col: %{customdata[1]}',
        'Latent row: %{customdata[2]}',
        'Group: %{customdata[3]}',
        'Class: %{customdata[4]}'])
    i = 5
    for anomaly_score in anomaly_scores:
        hovertemplate += '<br>' + \
            ui.split_and_capitalize(anomaly_score) + \
            f': %{{customdata[{i}]:.5f}}'
        i += 1

    fig = px.imshow(
        samples[columns].values.reshape(expected_shape[1:]),
        aspect='equal', animation_frame=0)

    # TODO: Support update frames of animations (name, hovertemplate...)
    # for inputs and all other traces frames below.
    fig.update_traces(
        selector=0,
        name='Input',
        customdata=custom_data[0],
        visible=True,
        showlegend=True,
        hovertemplate=hovertemplate)

    cols = [f'{col}_rec' for col in columns]
    reconstructed_traces = px.imshow(
        samples[cols].values.reshape(expected_shape[1:]),
        aspect='equal', animation_frame=0)

    reconstructed_traces.update_traces(
        selector=0,
        name='Reconstructed',
        hovertemplate=None,
        visible='legendonly',
        showlegend=True,
    )

    fig.add_traces(reconstructed_traces.data)

    cols = [f'{col}_equispaced' for col in columns]
    equispaced_traces = px.imshow(
        samples[cols].values.reshape(expected_shape[1:]),
        aspect='equal', animation_frame=0)

    equispaced_traces.update_traces(
        selector=0,
        name='Mean of decoded equispaced latent samples',
        hovertemplate=None,
        visible='legendonly',
        showlegend=True,
    )

    fig.add_traces(equispaced_traces.data)

    cols = [f'{col}_mean' for col in columns]
    mean_traces = px.imshow(
        samples[cols].values.reshape(expected_shape[1:]),
        aspect='equal', animation_frame=0)

    mean_traces.update_traces(
        selector=0,
        name='Mean of inputs',
        hovertemplate=None,
        visible='legendonly',
        showlegend=True,
    )

    fig.add_traces(mean_traces.data)

    cols = [f'{col}_rec_error' for col in columns]
    error_traces = px.imshow(
        samples[cols].abs().values.reshape(expected_shape[1:]),
        aspect='equal', animation_frame=0)

    error_traces.update_traces(
        selector=0,
        name='Error',
        hovertemplate=None,
        visible='legendonly',
        showlegend=True,
    )

    fig.add_traces(error_traces.data)

    if 'sample_pixel_anomaly_score_pxseq' in samples:
        anomaly_score_pxseq_traces = px.imshow(
            samples['sample_pixel_anomaly_score_pxseq'].abs(
            ).values.reshape(expected_shape[1:]),
            aspect='equal', animation_frame=0)

        anomaly_score_pxseq_traces.update_traces(
            selector=0,
            name='Sample Pixel Anomaly Score per pixel',
            hovertemplate=None,
            visible='legendonly',
            showlegend=True,
        )

        fig.add_traces(anomaly_score_pxseq_traces.data)

    # Final layout
    fig.update_layout(
        autosize=True,
        height=700,
        width=700,
        margin=dict(b=0, t=20, l=0, r=0),
        template='simple_white',
        legend=dict(orientation='h', title=''),
        hovermode='x',
        coloraxis_colorscale='viridis',
        coloraxis_cauto=False,
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    fig.update_xaxes(visible=False)
    fig.update_yaxes(visible=False)

    return fig


def plot_histogram(dataframe, column, vertical_line_value, max_value):
    """Plot histogram for a given column in dataframe, adding a vertical line.
    The histogram is colored by "class" column of dataframe.

    :param dataframe: Dataframe with the column to be used for the histogram
        computation and the "class" column to be used for colors.
    :param column: Column to be plotted in the histogram.
    :param vertical_line_value: Value where vertical line is placed.
    :param max_value: Maximum value to plot in the histogram horizontal axis.
        Values beyond are clipped.
    :return: Histogram figure.
    """
    logging.info('Plotting histogram...')

    dataframe = dataframe.reset_index()
    max_value = max(max_value, vertical_line_value)
    dataframe['clipped'] = dataframe[column].clip(upper=max_value * 1.1)
    fig = px.histogram(dataframe,
                       x=['clipped'],
                       color='class',
                       hover_data={'variable': False})
    fig.update_traces(opacity=0.75, autobinx=False, xbins=dict(
        start=0, size=max_value/50, end=max_value * 1.2))
    fig.add_vline(x=vertical_line_value,
                  line_color='green',
                  line_dash='dash',
                  line_width=3,
                  annotation_text=round(vertical_line_value, 4),
                  annotation_font={'size': 20},
                  annotation_y=0.5,
                  annotation_yref='paper')

    fig.update_layout(
        barmode='overlay',
        autosize=True,
        showlegend=False,
        template='simple_white',
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    return fig


def plot_roc(roc):
    """Plot ROC with FPR vs TPR, with threshold in tooltip.

    :param roc: List of arrays, [fpr, tpr, thresholds]
    :return: ROC figure.
    """
    df = pd.DataFrame({'FPR': roc[0], 'TPR': roc[1], 'Threshold': roc[2]})
    fig = px.line(df, x='FPR', y='TPR',
                  hover_data={'FPR': ':.2%',
                              'TPR': ':.2%',
                              'Threshold': ':.4f'})
    fig.update_traces(opacity=0.75)
    fig.update_layout(
        barmode='overlay',
        autosize=True,
        showlegend=False,
        template='simple_white',
        paper_bgcolor='rgba(0,60,116,.5)',
        plot_bgcolor='rgba(0,60,116,0)'
    )
    return fig
