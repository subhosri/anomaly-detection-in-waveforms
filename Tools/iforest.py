import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as cm
import seaborn as sns
import scipy.io as spio
import pickle
import random
import mat73
import math
import pandas as pd 
import plotly.express as px
from datetime import datetime
from sklearn.model_selection import train_test_split, KFold
from sklearn.ensemble import IsolationForest
from sklearn.metrics import accuracy_score, confusion_matrix, ConfusionMatrixDisplay, roc_curve, auc, roc_auc_score
from sklearn.utils import shuffle

import warnings
warnings.filterwarnings("ignore")

mapping = {'1': 'Noise',
           '2': 'RandomCrossTalk',
           '3': 'DutyCycleDistortion',
           '4': 'RandomRunt',
           '5': 'RandomGlitch',
           '6': 'Reflections',
           '7': 'RandomJitter'}

def load_mat_data(filename_mat):
    datAll = spio.loadmat(filename_mat)

    dat = datAll['data']
    # create a multidim. array with dim.: (#images x #cols x #rows)
    dat_arr = np.ndarray((dat.shape[1], np.ndarray.item(datAll['PIC_COLS']), np.ndarray.item(datAll['PIC_COLS'])))
    # write the images in the multidim. array
    for i in range(dat.shape[1]):
        dat_arr[i - 1] = dat[0, i - 1]
    # reshape the images into vectors: (#images x #cols*#rows)
    dat = dat_arr.reshape((len(dat_arr), np.prod(dat_arr.shape[1:])))
    presequence = datAll['sequence']
    sequence = np.ndarray((presequence.shape[1], presequence[0, 0].shape[1]))
    for i in range(presequence.shape[1]):
        sequence[i] = presequence[0, i]
    distortion_parameters = datAll['distortion_parameters']

    return dat, datAll['NrOfTestPatternsPerDistortionType_vec'][0], np.ndarray.item(
        datAll['PIC_COLS']), distortion_parameters, sequence

def plot_hist(dat, DistortionType, NumberOfImages, NrOfTestPatternsPerDistortionType, PIC_COLS):
    NumberOfImages = min(NrOfTestPatternsPerDistortionType[DistortionType], NumberOfImages)
    fig = plt.figure(figsize=(20, int(math.ceil(NumberOfImages / 4.0)) * 4))
    plt.title('Distortion Type '+mapping[str(DistortionType+1)])
    for i in range(NumberOfImages):
        fig.add_subplot(int(math.ceil(NumberOfImages / 4.0)), 4, i + 1)
        plt.imshow(dat[i + sum(NrOfTestPatternsPerDistortionType[0:DistortionType-1])].reshape(PIC_COLS, PIC_COLS), cmap = 'gray')
        plt.xticks([])
        plt.yticks([])
    plt.show()
### Loading the data
path = r'//mnt//data//oscillo//new_param.mat'
data_28, test_pattern_dist_28, pic_cols_28, dist_parm_28, seq_28 = load_mat_data(path)

def plot_correlation(scores, y, parmeters, thresh):
    indices = [1, 2, 2, 3, 4, 5, 6, 6, 7]
    parm_names=['noise_std', 'CrossT_amp', 'CrossT_width', 'DC_Imb', 'RuntLen', 'NumGlitch', 'DelayRefl', 'AmplRefl', 'JitterStd']
    fig, axs = plt.subplots(3, 3, figsize=(20,20))
    for i in range(3):
        for j in range(3):
            index = indices[j+3*i]
            score = scores[np.where(y==index)[0]]
            param = parmeters[np.where(y==index)[0], j+3*i]
            axs[i, j].scatter(param, score)
            axs[i, j].set_title(parm_names[j+3*i])
            axs[i, j].axhline(thresh, color='r', linewidth=1, label = 'Threshold')
            axs[i, j].grid(True)
            axs[i, j].legend()
    plt.show()      

###Check the data distribution 
print('#### Data Distribution ####')
for i in range(1, len(mapping)+1):
    print(mapping[str(i)], ':', test_pattern_dist_28[i-1])
print('Total: ', sum(test_pattern_dist_28))

### build the labels
y = []
for i in range(1, len(mapping)+1):
    y.extend(test_pattern_dist_28[i-1]*[i])
#### data split between healthy data
X_train_hlth, X_test_hlth, y_train_hlth, y_test_hlth, dist_parm_28_train_hlth, dist_parm_28_test_hlth = train_test_split(data_28[:test_pattern_dist_28[0]], y[:test_pattern_dist_28[0]], dist_parm_28[:test_pattern_dist_28[0]], test_size=0.1)
#### data split between unhealthy data
X_train_out, X_test_out, y_train_out, y_test_out, dist_parm_28_train_out, dist_parm_28_test_out = train_test_split(data_28[test_pattern_dist_28[0]:], y[test_pattern_dist_28[0]:], dist_parm_28[test_pattern_dist_28[0]:], stratify=y[test_pattern_dist_28[0]:], test_size=0.2)

##### Test dat has both healthy and unhealthy data
X_test = np.concatenate([X_test_hlth, X_test_out])
y_test = np.concatenate([y_test_hlth, y_test_out])
dist_parm_28_test = np.concatenate([dist_parm_28_test_hlth, dist_parm_28_test_out])

y_test_red=[]
for i in y_test: 
    if i>1:
        y_test_red.append(-1)
    else:
        y_test_red.append(1)

#### to evaluate the performance
def compute_roc(y_true, y_pred):
    fpr, tpr, _ = roc_curve(y_true, y_pred)
    return fpr, tpr


#### isolation forest model
def experiment_iso(X_train, y_train,  alpha, n_estimators):
    train_times=[]
    inference_times = []
    tprs=[]
    aucs=[]
    aucs_test = []
    acc_score=[]
    
    y_train_red=[]
    for i in y_train: 
        if i>1:
            y_train_red.append(-1)
        else:
            y_train_red.append(1)
            
    fig, ax = plt.subplots(1, 3, figsize=(30,10))
    base_name = './models/iso/'
    for k in range(1):
        X_train,y_train_shu, y_train_red = shuffle(X_train, y_train, y_train_red)        
        iso_forest = IsolationForest(n_estimators=n_estimators, random_state=random.randint(1,100))    
        start=datetime.now()
        iso_forest.fit(X_train)
        train_time = datetime.now()-start
        
        y_pred = iso_forest.predict(X_train)
        start_test = datetime.now()
        y_test_pred = iso_forest.predict(X_test) 
        test_time = datetime.now()-start_test

        scores = iso_forest.score_samples(X_train) 
        ############        
        scores_test = iso_forest.score_samples(X_test)
        plot_correlation(scores_test, y_test, dist_parm_28_test)
#         plot_3D_correlation(scores_test, y_test, dist_parm_28_test)

        if k==0:
            confusion = confusion_matrix(y_train_red, y_pred)
            FP = confusion.sum(axis=0) - np.diag(confusion)  
            FN = confusion.sum(axis=1) - np.diag(confusion)
            TP = np.diag(confusion)
            TN = confusion.sum() - (FP + FN + TP)

            # Specificity or true negative rate
            tpr = TN/(TN+FP) 
            
            for i in range(1,len(mapping)+1):
                n, bins, patches = ax[1].hist(scores[np.array(y_train_shu)==i], 50, density=True, alpha=0.75, log=False, label=mapping
            ax[1].axvline(-0.5, color='b', linewidth=1, label = 'Threshold')
            ax[1].grid(True)
            ax[1].legend()
            ax[1].title.set_text('train scores')

            
            for i in range(1,len(mapping)+1):
                n, bins, patches = ax[2].hist(scores_test[np.array(y_test)==i], 50, density=True, alpha=0.75, log=False, label=mapping
              
            ax[2].axvline(-0.5, color='b', linewidth=1, label = 'Threshold')
            ax[2].grid(True)
            ax[2].legend()
            ax[2].title.set_text('test scores')

        fpr, tpr = compute_roc(y_train_red, scores)
        fpr_test, tpr_test = compute_roc(y_test_red, scores_test)

        aucs.append(roc_auc_score(y_train_red, scores))
        aucs_test.append(roc_auc_score(y_test_red, scores_test))
        
        
        if k==0:
            ax[0].plot(fpr, tpr, label='train ROC')
            ax[0].plot(fpr_test, tpr_test, label='test ROC')
        
        train_times.append(train_time)
        inference_times.append(test_time)
        tprs.append(tpr[1])
        acc_score.append(accuracy_score(y_train_red, y_pred))
        if k==0:
            pickle.dump(iso_forest, open(base_name+'iso_'+str(100*alpha)+'_'+str(len(y_train))+'_'+str(n_estimators)+'.sav', 'wb'))

    ax[0].title.set_text("ROC Curve "+str(len(X_train))+' samples, '+str(100*alpha)+'% anomalies '+ str(n_estimators) + ' estimators')
    ax[0].set_xlabel("FPR")
    ax[0].set_ylabel("TPR")
    ax[0].legend()
    return np.mean(train_times), np.mean(inference_times), np.mean(tprs), np.mean(acc_score), np.mean(aucs), np.mean(aucs_test)

#### hyper parameter tuning
for alpha in [0.01]:
    fig, ax = plt.subplots(1, 4, figsize=(30,10))
    if alpha>0:
        test_size = 1-int((alpha*len(y_train_hlth)/(1-alpha))/(len(mapping)-1))/test_pattern_dist_28[1]
        X_train_out_i, X_test_out_i, y_train_out_i, y_test_out_i, dist_parm_28_train_out_i, dist_parm_28_test_out_i = train_test_split(X_train_out, y_train_out, dist_parm_28_train_out, test_size=test_size, stratify=y_train_out)
        X_train_i = np.concatenate([X_train_hlth, X_train_out_i])
        y_train_i = np.concatenate([y_train_hlth, y_train_out_i])
    else:
        X_train_i= X_train_hlth
    
    for n_estimators in [50]: #[10, 30, 50, 70, 100, 150]:
        times_iso=[]
        times_iso_inf=[]
        accs=[]
        tprs=[]
        x=[]
        aucs=[]
        aucs_test=[]
        percent=[0.05, 0.2, 0.4, 0.6, 0.8, 1.0]
        for p in percent: 
            if p<1:
                X_train_sub,_, y_train_sub, _ = train_test_split(X_train_i, y_train_i, stratify=y_train_i, train_size=p)
                x.append(len(y_train_sub))
                time, times_test, tpr, acc, auc_iso, auc_iso_test =  experiment_iso(X_train_sub[np.argsort(y_train_sub)], y_train_sub[np.argsort(y_train_sub)], alpha, n_estimators)
            else:
                time, times_test, tpr, acc, auc_iso, auc_iso_test =  experiment_iso(X_train_i[np.argsort(y_train_i)], y_train_i[np.argsort(y_train_i)], alpha, n_estimators)
                x.append(len(y_train_i))

            times_iso.append(time.total_seconds())
            times_iso_inf.append(times_test.total_seconds())
            accs.append(acc)
            tprs.append(tpr)
            aucs.append(auc_iso)
            aucs_test.append(auc_iso_test)

        ax[0].plot(x, times_iso, '-o', label=str(n_estimators))
        ax[1].plot(x, times_iso_inf, '-o', label=str(n_estimators))
        ax[2].plot(x, aucs, '-o', label=str(n_estimators))
        ax[3].plot(x, aucs_test, '-o', label=str(n_estimators))

    ax[0].set_xlabel('Iso: Number of training samples ('+ str(alpha*100)+'% anomalies)')
    ax[0].set_ylabel('Training time')
    ax[0].legend(title='Num Estimators')

    ax[1].set_xlabel('Iso: Number of training samples ('+ str(alpha*100)+'% anomalies)')
    ax[1].set_ylabel('Inference time test set')
    ax[1].legend(title='Num Estimators')
    
    ax[2].set_xlabel('Iso: Number of training samples ('+ str(alpha*100)+'% anomalies)')
    ax[2].set_ylabel('AUC train')
    ax[2].legend(title='Num Estimators')

    ax[3].set_xlabel('Iso: Number of training samples ('+ str(alpha*100)+'% anomalies) ')
    ax[3].set_ylabel('AUC test')
    ax[3].legend(title='Num Estimators')
