import numpy as np
import scipy
from scipy.optimize import linear_sum_assignment
import tensorflow.compat.v1 as tf
import keras
from keras.layers import Input, Dense, Lambda, concatenate, Concatenate
from keras.losses import mse, binary_crossentropy
from keras.models import Model
from keras import backend as K


def sampling(tensors):
    
    z_mean = tensors[0]
    z_log_var = tensors[1]
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    epsilon = K.random_normal(shape = (batch, dim))
    
    return z_mean + K.exp(0.5 * z_log_var) * epsilon

def gen_VAE_3layer(input_dim, kl_weight, ce_weight, dim_array):
    
    # network parameters
    
    original_dim = input_dim
    input_shape = (original_dim, )
    intermediate_dim = dim_array[0]
    latent_dim = 2
    
    # VAE model = encoder + decoder
    # build encoder model
    
    inputs = Input(shape = input_shape, name = 'encoder_input')
    encoded = Dense(intermediate_dim, activation = 'relu')(inputs)
    z_mean = Dense(latent_dim, name = 'z_mean')(encoded)
    z_log_var = Dense(latent_dim, name = 'z_log_var')(encoded)

    # use reparameterization trick to push the sampling out as input
    
    z = Lambda(sampling, output_shape = (latent_dim,), name = 'z')([z_mean, z_log_var])
    
    # build decoder model
    
    latent_inputs = Input(shape = (latent_dim,), name = 'z_sampling')
    decoded = Dense(intermediate_dim, activation = 'relu')(latent_inputs)
    outputs = Dense(original_dim, activation = 'sigmoid')(decoded)
    
    # instantiate models
    
    encoder = Model(inputs, [z_mean, z_log_var, z], name = 'encoder')
    decoder = Model(latent_inputs, outputs, name = 'decoder')
    
    # connect all the layers
    
    outputs = decoder(encoder(inputs)[2])
    vae = Model(inputs, outputs, name = 'vae_mlp')
    
    # define loss function
    
    reconstruction_loss = K.sum(K.binary_crossentropy(inputs, outputs), axis = -1)    
    
    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis = -1)
    kl_loss *= -0.5
    
    vae_loss = K.mean(reconstruction_loss * ce_weight + kl_loss * kl_weight)
    vae.add_loss(vae_loss)
    
    vae.compile(optimizer = 'adam')
    vae.summary()
    
    return encoder, decoder, vae


def gen_VAE_5layer(input_dim, kl_weight, ce_weight, dim_array):
    
    # network parameters
    
    original_dim = input_dim
    input_shape = (original_dim, )
    intermediate_dim1 = dim_array[0]
    intermediate_dim2 = dim_array[1]    
    latent_dim = 2
    
    # VAE model = encoder + decoder
    # build encoder model
    
    inputs = Input(shape = input_shape, name = 'encoder_input')
    encoded = Dense(intermediate_dim1, activation = 'relu')(inputs)
    encoded = Dense(intermediate_dim2, activation = 'relu')(encoded)    
    z_mean = Dense(latent_dim, name = 'z_mean')(encoded)
    z_log_var = Dense(latent_dim, name = 'z_log_var')(encoded)

    # use reparameterization trick to push the sampling out as input
    
    z = Lambda(sampling, output_shape = (latent_dim,), name = 'z')([z_mean, z_log_var])
    
    # build decoder model
    
    latent_inputs = Input(shape = (latent_dim,), name = 'z_sampling')    
    decoded = Dense(intermediate_dim2, activation = 'relu')(latent_inputs)
    decoded = Dense(intermediate_dim1, activation = 'relu')(decoded)
    outputs = Dense(original_dim, activation = 'sigmoid')(decoded)
    
    # instantiate models
    
    encoder = Model(inputs, [z_mean, z_log_var, z], name = 'encoder')
    decoder = Model(latent_inputs, outputs, name = 'decoder')
    
    # connect all the layers
    
    outputs = decoder(encoder(inputs)[2])
    vae = Model(inputs, outputs, name = 'vae_mlp')

    # define loss function
    
    reconstruction_loss = K.sum(K.binary_crossentropy(inputs, outputs), axis = -1)

    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis = -1)
    kl_loss *= -0.5
    
    vae_loss = K.mean(reconstruction_loss * ce_weight + kl_loss * kl_weight)
    vae.add_loss(vae_loss)
    
    vae.compile(optimizer = 'adam')
    vae.summary()
    
    return encoder, decoder, vae


def gen_VAE_7layer(input_dim, kl_weight, ce_weight, dim_array):
    
    # network parameters
    
    original_dim = input_dim
    input_shape = (original_dim, )
    intermediate_dim1 = dim_array[0]
    intermediate_dim2 = dim_array[1]    
    intermediate_dim3 = dim_array[2]    
    latent_dim = dim_array[3]
    
    # VAE model = encoder + decoder
    # build encoder model
    
    inputs = Input(shape = input_shape, name = 'encoder_input')
    encoded = Dense(intermediate_dim1, activation = 'relu')(inputs)
    encoded = Dense(intermediate_dim2, activation = 'relu')(encoded)    
    encoded = Dense(intermediate_dim3, activation = 'relu')(encoded)    
    z_mean = Dense(latent_dim, name = 'z_mean')(encoded)
    z_log_var = Dense(latent_dim, name = 'z_log_var')(encoded)

    
    # use reparameterization trick to push the sampling out as input
    
    z = Lambda(sampling, output_shape = (latent_dim,), name = 'z')([z_mean, z_log_var])
    
    # build decoder model
    
    latent_inputs = Input(shape = (latent_dim,), name = 'z_sampling')        
    decoded = Dense(intermediate_dim3, activation = 'relu')(latent_inputs)
    decoded = Dense(intermediate_dim2, activation = 'relu')(decoded)
    decoded = Dense(intermediate_dim1, activation = 'relu')(decoded)
    outputs = Dense(original_dim, activation = 'sigmoid')(decoded)
    
    # instantiate models
    
    encoder = Model(inputs, [z_mean, z_log_var, z], name = 'encoder')
    decoder = Model(latent_inputs, outputs, name = 'decoder')
    
    # connect all the layers
    
    outputs = decoder(encoder(inputs)[2])
    vae = Model(inputs, outputs, name = 'vae_mlp')

    # define loss function
    
    reconstruction_loss = K.sum(K.binary_crossentropy(inputs, outputs), axis = -1)

    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis = -1)
    kl_loss *= -0.5
    
    vae_loss = K.mean(reconstruction_loss * ce_weight + kl_loss * kl_weight)
    vae.add_loss(vae_loss)
    
    vae.compile(optimizer = 'adam')
    vae.summary()
    
    return encoder, decoder, vae


