import scipy.io
import matplotlib.pyplot as plt
import time
import numpy as np

def getwidths(data, level1, hyst1):
    blevel1 = data > level1
    bhyst1 = data > hyst1

    # Find the locations where bhyst1 changes from 1 to 0 or 0 to 1
    edge_locations = np.where(np.diff(bhyst1.astype(int)) != 0)[0]

    # Ensure that edge_locations has an even number of elements
    if len(edge_locations) % 2 != 0:
        edge_locations = edge_locations[:-1]

    # Calculate the widths of the intervals between edge locations
    opwidths = edge_locations[1::2] - edge_locations[0::2]

    return opwidths


def get_multilevel_width_histogram(WHist, hyst_delta):
    ipdata = WHist['ipdata']
    vlevels = WHist['vlevels']
    vbins = WHist['vbins']
    vhysts = vlevels - hyst_delta
    Ym = np.zeros((len(vlevels), len(vbins)-1))

    for k in range(len(vlevels)):
        width_arr = getwidths(ipdata, vlevels[k], vhysts[k])
        Y, _ = np.histogram(width_arr, bins=vbins)
        Ym[k, :] = Y

    WHist['Ym'] = Ym
    return WHist

import numpy as np


strSignalFilename = 'C:/Users/basu_s/Documents/MATLAB codes/Clock_10MHz_with_anomalies.mat'

loadedData = scipy.io.loadmat(strSignalFilename)
vdWaveform = loadedData['vdWaveform']
vdWaveform = vdWaveform[:].flatten()

# dSamplingRate = 1 / sSignalParameters.Resolution
avg = np.mean(np.abs(vdWaveform) ** 2)
peak = np.sqrt(3 * avg)

# dSamplesPerSymbol = dSamplingRate / dSymbolRate
PIC_ROWS = 28

# vlevels_default = np.linspace(np.min(vdWaveform), np.max(vdWaveform), PIC_ROWS)
vlevels_default = np.linspace(-peak, peak, PIC_ROWS)
num_p = 5 ##no.of samples per symbol
vbins_default = np.linspace(1, 3*num_p, PIC_ROWS+1)
hyst_delta = 0.08 #this depends on the amplitude of the signal, lower the amplitude, lower should be hyst_delta

WHist = {
    'vlevels': vlevels_default,
    'vbins': vbins_default,
    'ipdata': vdWaveform,
}

data = []
sequence = []
segment_size = 200
num_segments = num_segments = len(vdWaveform) // segment_size
i = 0
from tqdm import tqdm
with tqdm(total=num_segments, desc='Creating segments') as pbar:
    for index in range(0, len(vdWaveform) - len(vdWaveform) % 100 - 100, segment_size):
        i += 1
        WHist['ipdata'] = vdWaveform[index:index + segment_size]
        sequence.append(vdWaveform[index:index + segment_size])
        thisHist = get_multilevel_width_histogram(WHist, hyst_delta)['Ym']
        thisdata = thisHist.flatten()
        thisdata = thisdata / np.max(thisdata)
        thisdata[thisdata != 0] = -np.log10(thisdata[thisdata != 0]) + 1
        thisdata = thisdata / np.max(thisdata)
        data.append(thisdata.reshape(PIC_ROWS, PIC_ROWS))
        pbar.update(1)
dat_arr = np.array(data)

dat = dat_arr.reshape((len(dat_arr), np.prod(dat_arr.shape[1:])))
